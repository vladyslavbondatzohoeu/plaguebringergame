# Plaguebringer.
Design document for Plaguebringer custom campaign for WarCraft 3: Frozen Throne game.

## Spells ideas.
Make plaguebringer manage the living, unlike other undead race hearoes, who manage and raise the dead.

Allow a choice between unholy and shadow magic. Make at least one alternative to each spell slot.
That is 8 spells in total.

### Crushing despair.
Deal 100 damage to target living enemy.
For each friendly undead unit under the control of the caster's owner,
deal 8/16/32 additional damage.
If a killing blow is delivered by the spell,
caster restores mana.

### Contaminate.
Create a void zone in selected area.
When a living unit dies while in the zone,
create a wraith summon.
Wraiths do not consume corpses, as to not interfere with the Amalgamation spell.
However it doesn't make sense to use corpse gas on them in that case.
Alternatively, make it create zombies that consume corpses,
but also leave corpses after they die.

### 4.1. Amalgamtion.
Ultimate unholly spell.
Described in depth elsewhere.
Basically combine corpses in an area of effect into one mega-abomination, 
that scales with the amount of corpses, and, possibly, inherits the abilities of all of the victims.

It can be entertaining for the player to seek out monsters and combine their abilities.

### 2.2. Blood spawn.
Shadow spell. Summons a blood spawn. 
Blood spawn can possess an enemy organic unit, possibly for a limited duration.
When the host is killed, the blood spawn re-emerges. However, the blood spawn has a limited lifespan.
If it can't take over some living unit, it will die, mana used to summon it wasted.

Basically a fresh spin on possessing enemy unit 
as well as summons (adds), which every intellect (caster) hero has.

Originally the idea was to make it a passive ability that units have.
If a unit with this ability dies,
a blood spawn emerges at their corpse.
If the blood spawn lives long enough and stays at the corpse,
and the corpse itself remains intact,
the unit will  be recovered with a portion of their health.

### 4.2. Curse of flesh.
Shadow spell. Possibly an ultimate one.
Cause damage to all mechanical units and possibly trees in the area of effect. 
Depending on the amount of damage done, spawn tentacles.

The spell is here mostly for flavour. It has instant effects, which can be entertaining.

### 3.2. Mind worm.
Shadow spell. Summons a mind worm.
When a magic user - friend or foe - casts a spell near the mind worm,
the mind worm grows in size, receiving more maximum health and
attack power.
However, mind worm gets slower as it grows, 
making it more difficult to stick to enemy casters.
Possibly deteriorate over time, losing health.

Just had a wild idea on spell countering abilities.
I had a mind-rot spell in my other campaign, 
that drained the enemy caster's mana,
as a mandatory custom anti-caster spell.
It was boring to use against the AI,
so this spell is different.

### 2.1. Inject plague.
Unholy spell.
Applies plague effect to a target organic unit.
Given the target was a friendly undead, then any organic unit that strike them suffer damage.
(Basically grant a potent thorns effects to a friendly target).
Given the target was an enemy organic unit, then any undead friendly unit that strike them restore health.
(Basically limited and possibly more potent vampirism on demand; or a hunter's mark type of thing).

Basically a spin on armor spells.
This spell is aimed to support the concept of spells being cast when appropriate, 
like frost armor,
instead of ASAP, like frost nova, blizzard, rain of fire, or even death coil.
When player units need healing, players cast it on enemy organic units.
If enemy only has mechanical units, the player is challenged.
Like when attacking fortifications or repelling base assault with siege machines.

Otherwise it can protect some friendly units from being focused. 

### 3.1. Seed of corruption.
Unholy spell.
When cast on a friendly undead unit, 
then grant the target the ability to cast corruption spell.
Corruption spell deals area of effect damage to enemies,
and healing the caster from the amount of damage done. 
However, it uses caster's health instead of mana.
Also, target under the effects of seed of corruption deteriorate, losing health over time.

This allows for more micro-management. Could be just an annoying chore the player has to do,
instead of casting some AoE directly.

### Exaltation.
A unit spell. Possibly grant it to mind worm when it grows to certain size.
Kills the caster and restores mana to the target friendly magic user.
Maybe make it mana-shield instead of mana recovery.

### Mind worm.
Whenever a spell is cast nearby, mind worm is healed slightly. 
Mind worm can cast dispel at the cost of one's health.
So to keep dispelling he needs casters to keep casting.
Friendly casters count.

### Void shield.
Unit ability. When cast on a friendly target,
grants them a shield for 30 seconds.
The shield redirects all damage taken by the target to the caster.
Additionally, the caster becomes hidden from the game,
until effect's expiration or until too much damage is taken.

### Shadow Lash.

### Harpoon.
Harpoon and drag first collided unit.

### Spell to swap palces with a ground unit.
Basically the mechanic is as follows.
Make shadowfiend void shield a flying or amphibious unit.
Make that unit bypass obstacles.
Let it "unload" the Shadowfiend.
Then swap places with the unloaded shadowfiend.
Either make the swap killing the target unit,
Alternatively make it actual swap of units,
but then restrict the range.

## Miscellaneous notes.

### Unsorted.
Make "Bring them peace!" Volgoff's battle cry.

### Final map thoughts. 
Night elves entrenched themselves at Mythirn's rest.
Volgoff orders to dump black blood into the river that leads to the location.
That way, the giant will be corrupted without Volgoff needing to fight the elves.
However, orcs and tauren show up and destroy most of the supplies.
Therefore, the amount of black blood remaining wasn't enough to finish the job.
Then, Volgoff decides to absorb the last remaining cistern of black blood into himself.
This transforms the plaguebringer into a hideous monstrosity.
He then swims to the Mythirn's rest, delivering the last needed black blood that way.
Player would need to keep Volgoff alive at Mythirn's rest for certain amount of time to win.

Make river change colour as pollution progresses.
Possibly make it possible to sacrifice mutated units to speed up pollution.

Possibly make orcs fight night elves.
The reason would be to deny night elves the power of Mythirn.
Because orcs suspect that the elves will use the giant against them.
The complication is to make orcs aware of Mytirn. 
Also, elves and orcs should be allied at this point in time.
However, the conflict between elves and orcs 
would help to sell the narrative.
That is if only elves and orcs trusted each-other, worked together,
Volgoff would have failed.
The reason for orc's hostility could be violation of borders,
and performing unknown powerful magic on top of that.
Actually, it would help explaining orc military presence in the region.
They were sent to chase down the elves who violated the border.

Complication with the night elves.
They were supposed to not know of the location of Myhirn's rest,
much less how to awaken him.
So it doesn't make sense for them to be there.

Complication with the river.
It doesn't make sense for there to be a river.
Mythirn's rest should be plain or rocky terrain.

Tell me if it sounds cool. Druid sacred site is a small island in the middle of the lake. 
The undead bombard the site across the water by hurling containers with corruption. 
There are also undead flying beasts, probably raised night elf hyppogryphs, 
that just fly across the lake to fall down onto the island. 
They carry corruption in themselves, so when they crash they spread it that way. 
Maybe there are undead murlocs that do the same. 
The trees on the island wither and die as the onslaught continues. 
But night elf druids heal the damage to some extent from another side of the lake. 
Restoring trees and summoning ancient protectors. 
So undead try to hunt and kill them. 
In the end, realising that the night elves will hold on if the siege continues, 
undead hero absorbs corruption into himself, 
turns into a monster, swims across the lake and dies there to ensure corruption took hold.

### Final mission objectives.
Mythirn's rest is a island in the middle of a tiny lake.
The focal point is lush plant life on the island,
in contrast to barren plain outside the lake.
The resting site is guarded by powerful ancient protectors.
The protectors hurl massive boulders on the attackers.
However, there are dead zones.
Those dead zones are the objectives of the player.
While in the deadzone, player can use some kind of unit to 
bombard the sacred site with corruption from safety.
Night elves can also use those deadzones to heal the protectors.
This inroduces recovery damage capability to the computer enemy.
This way it is much more challenging to the player,
befiting the final mission.
Introduce orc reinforcements as a soft time limit.
Orc reinforcements are unlimited and they constantly push.
Probably.
Anyway, use it as a story device to explain why undead can't retreat now.
Orcs are coming and the ship is blown.

Instead of meatwagons, use repurposed ship cannons.

### First mission objectives.
Ship must move much slower than the troops.
Otherwise, player will have portable cover,
and the risking of exploring with the ship is nullified.

### Spell selection.
Spell selection map will be called Volgoff's Nightmare.
The theme is that this is him resting and dreaming between missions.
And as the player progresses through the chapters,
different sections of the nightmare open up.
The content of the nightmare is him remembering 
how he joined the Cult of the Damned.
His village was destroyed by Grubux's forces.
After that Kel'Thuzad found him, still as human,
and made him an apprentice.
The nightmare progresses as follows.
First peaceful city,
then orc attack, then orc occupy the city,
and the ultimate is confrontation with Grubux.
May add the plotline of orcs sacrificing his fiancee.
And underline that Grubux was eating corpses.
Each section of the dream is a choice between
shadow or unholy magic.
Volgoff seeks peace of mind after the horrors he witnessed,
so he joined the cult to gain indifference and oblivion,
to become undead.
Maybe add image of Kel'Thuzad at the end.
Also, having this set up this way is also important,
to create playground for testing new spells out.
Additionaly, Volgoff will comment differently,
depending on the section of the nightmare,
story progression,
and spell choice.

### Powerups.
It is boring when enemies in a tactical mission drop no power ups.
Player needs to have a lot of immediate rewards, like in TFT campaign.
But using vanilla runes is boring.
Instead, create new powerups, like "Fish Skull" or "Scourge Token",
that instead of replenishing resources grant another unit.
Undead units have a lot of replenishment as-is.
It's flavourful and works for the mechanic.
Fish Skull can grant a Mar'Ghoul and Scourge Token a shade or smth idk.

## Issues.
- [x] Issue 40. Indicate safe areas with torches in the first chapter.
- [x] Issue 39. Make barrier for the sea in the first chapter.
- [ ] Issue 38. Make sure area of effect of Shadow Lash spell lashes is rectangular.
- [ ] Issue 37. Fix area offset in Shadow Lash spell.
- [x] Issue 36. Make exit painfully obvious.
- [x] Issue 35. Add optional quest for rescuing necromancers.
- [x] Issue 34. Add indicators to the first map.
- [x] Issue 33. Fix rescued necromancer's dialog.
- [x] Issue 32. Fix AI in first chapter.
- [x] Issue 31. Implement Shadow Lash spell.
- [ ] Issue 30. Imagine mechanic for the second chapter.
- [ ] Issue 29. Make topdown sketch for the second chapter.
- [ ] Issue 28. Make pace plot for the second chapter.
- [ ] Issue 27. Make flowchart for the second chapter.
- [x] Issue 26. When a player attempts to save the game in the first chapter, the game crashes.
- [x] Issue 25. Make corpse gas spell effect to cancel when spell target is hidden from the game.
- [x] Issue 24. Add campaign loading screen.
- [x] Issue 23. Add intro loading screen.
- [x] Issue 22. Fix transmission error in intro to first chapter.
- [x] Issue 21. Fix void shield bug when cast on timed life unit.
- [ ] Issue 20. Add custom model for Contamination spell summon.
- [ ] Issue 19. Add custom model for Shadowfiend.
- [x] Issue 18. Add foam for chapter one.
- [ ] Issue 17. Make custom rocks model for chapter one.
- [x] Issue 16. Add persistence.
- [x] Issue 15. Add lightning strike effects. Maybe also add weird shadows.
- [x] Issue 14. Create outro for first chapter.
- [x] Issue 13. Implement spell tree switching.
- [x] Issue 12. Apply custom graphics to corpse gas spell.
- [x] Issue 11. Apply custom graphics to void shield spell.
- [x] Issue 10. Add difficulty changing dialog.
- [x] Issue 9. Add hints, secrets and quests API. Then use it at first chapter.
- [x] Issue 8. Add first chapter loading screen.
- [x] Issue 7. Make corpse gas destroy trees.
- [x] Issue 6. Ensure health doesn't drop while under void shield.
- [x] Issue 5. Implement Void Shield spell.
- [x] Issue 4. Change flavour of Contaminate summon's from ethereal to flesh. For example oozes.
- [x] Issue 3. Change Corpse Gas damage text tag to factor in the quantity of affected enemies.
- [x] Issue 2. Change impact effect of Corpse Gas to Carrion Swarm effect.
- [x] Issue 1. Implement Contaminate spell.
