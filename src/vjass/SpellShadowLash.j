// Begin SpellShadowLash.j
// Version: 1.0
globals
    constant string SSL_EFFECT = "Abilities\\Weapons\\AvengerMissile\\AvengerMissile.mdx"
    constant integer SHADOW_LASH = 'A60L'

    constant real SSL_MARGIN = 32.0
    constant real SSL_LASH_DELAY = 1.5
    constant real SSL_LASH_HEIGHT = 64.0 

    integer sslLightningQuantity = 0
    lightning array sslLightning
    real array sslLightningOriginX
    real array sslLightningOriginY
    real array sslLightningSpeedX
    real array sslLightningSpeedY
    real array sslLightningTargetX
    real array sslLightningTargetY
    real array sslLightningX
    real array sslLightningY
    timer array sslLightningTimer
endglobals

function SSLLightningTimerCallback takes nothing returns nothing
    local timer t = GetExpiredTimer() 
    local real timeout = TimerGetTimeout(t)
    local integer i = 0
    local integer j = 0
    local boolean break = false
    local real x1 = 0.0
    local real y1 = 0.0
    local real x = 0.0
    local real y = 0.0
    local real distRemaining = 0.0

    loop
        exitwhen j >= sslLightningQuantity or break
	if t == sslLightningTimer[j] then
	    set i = j 
	    set x = sslLightningX[i]
	    set y = sslLightningY[i]
	    set x1 = sslLightningTargetX[i]
	    set y1 = sslLightningTargetY[i]
	    set break = true
	endif
	set j = j + 1
    endloop

    set x = x + sslLightningSpeedX[i]*timeout
    set y = y + sslLightningSpeedY[i]*timeout
    set sslLightningX[i] = x
    set sslLightningY[i] = y
    call MoveLightningEx(sslLightning[i], true, sslLightningOriginX[i], sslLightningOriginY[i], 256.0, x, y, 0.0)
    call DestroyEffect(AddSpecialEffect(SSL_EFFECT, x, y))

    set distRemaining = SquareRoot((x1 - x)*(x1 - x) + (y1 - y)*(y1 - y))
    //call BJDebugMsg(I2S(i) + ": " + R2S(distRemaining))
    if distRemaining <= 8.0 then
        //call BJDebugMsg("Kill " + I2S(i))
        call DestroyTimer(t)
	call DestroyLightning(sslLightning[i])
	set sslLightningQuantity = sslLightningQuantity - 1
	
	set sslLightning[i] = sslLightning[sslLightningQuantity] 
        set sslLightningOriginX[i] = sslLightningOriginX[sslLightningQuantity]  
        set sslLightningOriginY[i] = sslLightningOriginY[sslLightningQuantity]  
        set sslLightningSpeedX[i] = sslLightningSpeedX[sslLightningQuantity]  
        set sslLightningSpeedY[i] = sslLightningSpeedY[sslLightningQuantity]  
        set sslLightningTargetX[i] = sslLightningTargetX[sslLightningQuantity]  
        set sslLightningTargetY[i] = sslLightningTargetY[sslLightningQuantity]  
        set sslLightningX[i] = sslLightningX[sslLightningQuantity]  
        set sslLightningY[i] = sslLightningY[sslLightningQuantity]  
        set sslLightningTimer[i] = sslLightningTimer[sslLightningQuantity]  
    endif

    set t = null
endfunction

function SSLAddLightning takes real x0, real y0, real x1, real y1, real x2, real y2 returns nothing
    local integer i = sslLightningQuantity
    local real magnitude = SquareRoot((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1))

    if i >= JASS_MAX_ARRAY_SIZE then
        return
    endif
    set sslLightning[i] = AddLightningEx("MBUR", true, x0, y0, 256.0, x1, y1, 0.0)
    set sslLightningOriginX[i] = x0
    set sslLightningOriginY[i] = y0
    set sslLightningX[i] = x1
    set sslLightningY[i] = y1
    set sslLightningTargetX[i] = x2
    set sslLightningTargetY[i] = y2
    set sslLightningSpeedX[i] = ((x2 - x1)/magnitude)*1024.0
    set sslLightningSpeedY[i] = ((y2 - y1)/magnitude)*1024.0
    //call BJDebugMsg("lightningSpeed: " + R2S(sslLightningSpeedX[i]) + ", " + R2S(sslLightningSpeedY[i]))
    set sslLightningTimer[i] = CreateTimer()
    call SetLightningColor(sslLightning[i], 1.0, 0.0, 0.5, 1.0)
    call TimerStart(sslLightningTimer[i], 0.05, true, function SSLLightningTimerCallback)
    set sslLightningQuantity = sslLightningQuantity + 1
endfunction

function SSLGetTriangleArea takes real ax, real ay, real bx, real by, real cx, real cy returns real
    local real abm = SquareRoot((bx - ax)*(bx - ax) + (by - ay)*(by - ay))
    local real acm = SquareRoot((cx - ax)*(cx - ax) + (cy - ay)*(cy - ay))
    local real bcm = SquareRoot((cx - bx)*(cx - bx) + (cy - by)*(cy - by))
    local real s = (abm + acm + bcm)*0.5
    local real triangleArea = SquareRoot(s*(s - abm)*(s - acm)*(s - bcm))
    
    //call BJDebugMsg("triangleArea: " + R2S(triangleArea))
    return triangleArea
endfunction

function SSLIsPointInsideRect takes real x, real y, real x0, real y0, real x1, real y1, real x2, real y2, real x3, real y3 returns boolean
    local real a = SSLGetTriangleArea(x0, y0, x1, y1, x, y) 
    local real b = SSLGetTriangleArea(x1, y1, x2, y2, x, y)
    local real c = SSLGetTriangleArea(x2, y2, x3, y3, x, y)
    local real d = SSLGetTriangleArea(x3, y3, x0, y0, x, y)
    local real width = SquareRoot((x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0))
    local real height = SquareRoot((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1))
    local real trianglesArea = a + b + c + d
    local real rectArea = width*height
    local real dist = trianglesArea - rectArea
    //call BJDebugMsg("triangleAreas: " + R2S(trianglesArea))
    //call BJDebugMsg("rectArea: " + R2S(rectArea))
    //call BJDebugMsg("dist: " + R2S(dist))
    return dist < 1.0 
endfunction

function P2S takes real x, real y returns string
    return " (" + R2S(x) + ", " + R2S(y) + ") "
endfunction

// TODO
function SSLIsCircleLashed takes real x, real y, real r, real x0, real y0, real x1, real y1, real x2, real y2, real x3, real y3 returns boolean
    local boolean result = false 
    local integer edgeQuantity = 4
    local integer i = 0
    local integer j = 0
    local real array vx
    local real array vy
    local real ax = 0.0
    local real ay = 0.0
    local real aDirX = 0.0
    local real aDirY = 0.0
    local real aMagnitude = 0.0
    local real eDirX = 0.0
    local real eDirY = 0.0
    local real eMagnitude = 0.0
    local real ex = 0.0
    local real ey = 0.0
    local real pMagnitude = 0.0
    local real px = 0.0
    local real py = 0.0
    local real mx = 0.0
    local real my = 0.0
    local real mDirX = 0.0
    local real mDirY = 0.0
    local real mMagnitude = 0.0
    local real scalar = 0.0

    set vx[0] = x0
    set vy[0] = y0
    set vx[1] = x1
    set vy[1] = y1
    set vx[2] = x2
    set vy[2] = y2
    set vx[3] = x3
    set vy[3] = y3

    //call BJDebugMsg("============")
    //call BJDebugMsg("r: " + R2S(r))
    //call BJDebugMsg("(x, y): " + P2S(x, y))
    loop
	set j = i + 1 
	if j >= 4 then
	    set j = 0
	endif
        exitwhen i >= edgeQuantity
	set ex = vx[j] - vx[i]
	set ey = vy[j] - vy[i]
	set eMagnitude = SquareRoot(ex*ex + ey*ey)
	set eDirX = ex/eMagnitude
	set eDirY = ey/eMagnitude
	set ax = vx[i] - x
	set ay = vy[i] - y
	set aMagnitude = SquareRoot(ax*ax + ay*ay)
	set aDirX = ax/aMagnitude
	set aDirY = ay/aMagnitude
	set scalar = ax*ex + ay*ey
	//set scalar = aDirX*eDirX + aDirY*eDirY 
	if scalar <= 0 then
	    set mx = vx[i]
	    set my = vy[i]
	elseif scalar >= eMagnitude then
            set mx = vx[j]
	    set my = vy[j]
	else

	set px = vx[i] + eDirX*scalar
	set py = vy[i] + eDirY*scalar
	set pMagnitude = SquareRoot(px*px + py*py)
	set mx = vx[i] + px
	set my = vy[i] + py

	endif
	set mMagnitude = SquareRoot(mx*mx + my*my)
	set mDirX = mx/mMagnitude
	set mDirY = my/mMagnitude

        //call AddLightning("CLPB", true, x, y, mx, my)
	//call BJDebugMsg("amMag: " + R2S(SquareRoot((mx - x)*(mx - x) + (my - y)*(my - y))))
	if SquareRoot((mx - x)*(mx - x) + (my - y)*(my - y)) <= r then
	    set result = true or result
	endif

        //call BJDebugMsg("===")
	//call BJDebugMsg("i: " + P2S(vx[i], vy[i]))
	//call BJDebugMsg("j: " + P2S(vx[j], vy[j]))
	//call BJDebugMsg("e: " + P2S(ex, ey))
	//call BJDebugMsg("eMag: " + R2S(eMagnitude))
	//call BJDebugMsg("eDir: " + P2S(eDirX, eDirY))
	//call BJDebugMsg("a: " + P2S(ax, ay))
	//call BJDebugMsg("aMag: " + R2S(aMagnitude))
	//call BJDebugMsg("aDir: " + P2S(aDirX, aDirY))
	//call BJDebugMsg("scalar: " + R2S(scalar))
	//call BJDebugMsg("p: " + P2S(px, py))
	//call BJDebugMsg("pMag: " + R2S(pMagnitude))
	//call BJDebugMsg("m: " + P2S(mx, my))
	//call BJDebugMsg("mDir: " + P2S(mDirX, mDirY))
	//call BJDebugMsg("mMag: " + R2S(mMagnitude))


	set i = i + 1
    endloop

    return result
endfunction

function SSLCauseLashDamageWithDelay takes unit caster, real x, real y, real dirX, real dirY, real dmg, real lashWidth, real delay returns nothing
    local player owner = GetOwningPlayer(caster)
    local unit f = null
    local real fx = x
    local real fy = y
    local real lowX = x - dirX*SSL_LASH_HEIGHT*0.5 
    local real lowY = y - dirY*SSL_LASH_HEIGHT*0.5 
    local real upX = x + dirX*SSL_LASH_HEIGHT*0.5 
    local real upY = y + dirY*SSL_LASH_HEIGHT*0.5
    local real ax = x + dirY*lashWidth*0.5 + dirX*SSL_LASH_HEIGHT*0.5
    local real ay = y - dirX*lashWidth*0.5 + dirY*SSL_LASH_HEIGHT*0.5
    local real x0 = x + (dirY)*lashWidth*0.5 + dirX*SSL_LASH_HEIGHT*0.5
    local real y0 = y + (-dirX)*lashWidth*0.5 + dirY*SSL_LASH_HEIGHT*0.5
    local real x1 = x + (dirY)*lashWidth*0.5 - dirX*SSL_LASH_HEIGHT*0.5
    local real y1 = y + (-dirX)*lashWidth*0.5 - dirY*SSL_LASH_HEIGHT*0.5
    local real x2 = x - (dirY)*lashWidth*0.5 - dirX*SSL_LASH_HEIGHT*0.5
    local real y2 = y - (-dirX)*lashWidth*0.5 - dirY*SSL_LASH_HEIGHT*0.5
    local real x3 = x - (dirY)*lashWidth*0.5 + dirX*SSL_LASH_HEIGHT*0.5
    local real y3 = y - (-dirX)*lashWidth*0.5 + dirY*SSL_LASH_HEIGHT*0.5

    local group g = CreateGroup()
    local effect e = null

    //call AddLightning("CLPB", true, x0, y0, x1, y1)
    //call AddLightning("CLPB", true, x1, y1, x2, y2)
    //call AddLightning("CLPB", true, x2, y2, x3, y3)
    //call AddLightning("CLPB", true, x3, y3, x0, y0)

    call GroupEnumUnitsInRange(g, x, y, SquareRoot(lashWidth*lashWidth + SSL_LASH_HEIGHT*SSL_LASH_HEIGHT)*0.5, null)

    //call BJDebugMsg("## Lash at (" + R2S(x) + ", " + R2S(y) + ").")
    //call BJDebugMsg(R2S(x0) + " " + R2S(y0) + " " + R2S(x1) + " " + R2S(y1) + " " + R2S(x2) + " " + R2S(y2) + " " + R2S(x3) + " " + R2S(y3))
    loop
        set f = FirstOfGroup(g)
	exitwhen null == f
	set fx = GetUnitX(f)
	set fy = GetUnitY(f)
	//call BJDebugMsg(GetUnitName(f) + " is affected.")
        if IsUnitAliveBJ(f) then//and SSLIsCircleLashed(fx, fy, 91.0, x0, y0, x1, y1, x2, y2, x3, y3) then
	    //call BJDebugMsg("Point inside rect!")
	    call UnitDamageTargetBJ(caster, f, dmg, ATTACK_TYPE_MAGIC, DAMAGE_TYPE_NORMAL)
	    set e = AddSpecialEffectTarget(SSL_EFFECT, f, "origin")
            call DestroyEffect(e)
	//else
	    //call BJDebugMsg("Miss.")
	endif
	call GroupRemoveUnit(g, f)
    endloop

    call DestroyGroup(g)
    set g = null

    call SSLAddLightning(GetUnitX(caster), GetUnitY(caster), x0, y0, x3, y3)

    set owner = null

    set e = null
endfunction

function CreateSpellEffectShadowLash takes unit caster, real x, real y, real dmg, integer lashes, real lashWidth returns nothing
    local real x0 = GetUnitX(caster)
    local real y0 = GetUnitY(caster)
    local real magnitude = SquareRoot((x - x0)*(x - x0) + (y - y0)*(y - y0))
    local real lashOffset = magnitude/((SquareRoot(I2R(lashes*lashes)) + lashes)/2.0 + 1.0)
    local real dirX = (x - x0)/magnitude
    local real dirY = (y - y0)/magnitude
    local integer i = 0
    local real toggle = 0.0
    local real lx = 0.0
    local real ly = 0.0

    //call BJDebugMsg("# Started lashing...")
    loop
        exitwhen i >= lashes
	if toggle > 0.0 then
	    set toggle = -1.0
	else
	    set toggle = 1.0
	endif
	set lx = x + (toggle*dirX)*lashOffset*I2R(i)
	set ly = y + (toggle*dirY)*lashOffset*I2R(i)
	call SSLCauseLashDamageWithDelay(caster, lx, ly, dirX, dirY, dmg, lashWidth, SSL_LASH_DELAY*I2R(i))
	set i = i + 1
    endloop
endfunction

function SSLOnCastFilter takes nothing returns boolean
    return SHADOW_LASH == GetSpellAbilityId()
endfunction 

function SSLOnCastCallback takes nothing returns nothing
    local real x = GetSpellTargetX()
    local real y = GetSpellTargetY()
    call CreateSpellEffectShadowLash(GetSpellAbilityUnit(), x, y, 50.0, GetUnitAbilityLevel(GetSpellAbilityUnit(), GetSpellAbilityId()), 256.0) 
endfunction

function InitSpellShadowLash takes nothing returns nothing
    local trigger t = CreateTrigger()

    call TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_SPELL_EFFECT)
    call TriggerAddCondition(t, Condition(function SSLOnCastFilter))
    call TriggerAddAction(t, function SSLOnCastCallback)

    set t = null
endfunction
// End SpellShadowLash.j
