/* SpellVoidShield.j 1.0 */
globals
  constant string VOID_SHIELD_EFFECT_TARGET = "war3campImported\\assets\\JetFangInferno\\DarkVoid.mdx"
  constant string VOID_SHIELD_EFFECT_IMPACT = "Abilities\\Weapons\\AvengerMissile\\AvengerMissile.mdx"
  constant integer VOID_SHIELD = 'A602'
  constant real VOID_SHIELD_DURATION = 30.0

  effect array SVSEffects
  real array SVSTargetMinLife
  timer array SVSTimerDuration
  trigger array SVSDamageTriggers
  unit array SVSCasters
  unit array SVSTargets

  trigger SVSDamageTrigger = null
  group SVSTargetsGroup = null
  integer SVSQuantity = 0
endglobals

function SVSDestroyEffectByIndex takes integer i returns nothing
  local boolean uniqueTarget = true 
  local integer j = 0
  local location targetLoc = null
  
  loop
    exitwhen j >= SVSQuantity or (not uniqueTarget)
    if SVSTargets[j] == SVSTargets[i] and i != j then
      set uniqueTarget = false
      call EnableTrigger(SVSDamageTriggers[j])
    endif
    set j = j + 1
  endloop

  if uniqueTarget then
    call GroupRemoveUnit(SVSTargetsGroup, SVSTargets[i])
  endif

  set SVSQuantity = SVSQuantity - 1
  set targetLoc = GetUnitLoc(SVSTargets[i])
  call SetUnitPositionLoc(SVSCasters[i], targetLoc)
  call ShowUnit(SVSCasters[i], true)

  call DestroyEffect(SVSEffects[i])
  set SVSEffects[i] = SVSEffects[SVSQuantity]
  set SVSTargetMinLife[i] = SVSTargetMinLife[SVSQuantity]
  set SVSCasters[i] = SVSCasters[SVSQuantity]

  call TriggerClearActions(SVSDamageTriggers[i])
  call TriggerClearConditions(SVSDamageTriggers[i])
  call DestroyTrigger(SVSDamageTriggers[i])
  set SVSDamageTriggers[i] = SVSDamageTriggers[SVSQuantity]

  set SVSTargets[i] = SVSTargets[SVSQuantity]
  call DestroyTimer(SVSTimerDuration[i])
  set SVSTimerDuration[i] = SVSTimerDuration[SVSQuantity]

  set SVSEffects[SVSQuantity] = null
  set SVSTargetMinLife[SVSQuantity] = 0.0
  set SVSCasters[SVSQuantity] = null
  set SVSDamageTriggers[SVSQuantity] = null
  set SVSTargets[SVSQuantity] = null
  set SVSTimerDuration[SVSQuantity] = null

  call RemoveLocation(targetLoc)
  set targetLoc = null


endfunction

function SVSOnDeathFilter takes nothing returns boolean
  return IsUnitInGroup(GetDyingUnit(), SVSTargetsGroup)
endfunction

function SVSOnDeathCallback takes nothing returns nothing
  local unit u = GetDyingUnit()
  local integer i = 0
  loop
    exitwhen i >= SVSQuantity 
    if u == SVSTargets[i] then
      call SVSDestroyEffectByIndex(i)
      set u = null
      return
    elseif u == SVSCasters[i] then
      call SetUnitPosition(u, GetUnitX(SVSTargets[i]), GetUnitY(SVSTargets[i]))
      call SVSDestroyEffectByIndex(i)
    endif
    set i = i + 1
  endloop
  set u = null
endfunction

function SVSOnDamageFilter takes nothing returns boolean
  return IsUnitInGroup(GetTriggerUnit(), SVSTargetsGroup) 
endfunction

function SVSOnDamageCallback takes nothing returns nothing
  local effect e = null
  local integer i = 0
  local real dmg = GetEventDamage()
  local trigger trg = GetTriggeringTrigger()
  local unit c = null
  local unit f = GetEventDamageSource()
  local unit t = GetTriggerUnit()
  local real fx = GetUnitX(f)
  local real fy = GetUnitY(f)
  local real tx = GetUnitX(t)
  local real ty = GetUnitY(t)
  local real distance = SquareRoot((tx - fx)*(tx - fx) + (ty - fy)*(ty - fy))
  local real ex = 0.0 
  local real ey = 0.0 
  
  loop
    exitwhen i >= SVSQuantity or (c != null)
    if trg == SVSDamageTriggers[i] then
      set c = SVSCasters[i]
    else
      set i = i + 1
    endif
  endloop

  /* Game version: 1.30.4 */
  call BlzSetEventDamage(0.0)
  /* End game version: 1.30.4 */

  call SetUnitPosition(c, GetUnitX(t), GetUnitY(t))
    /* Use damage instead of stat modification,
    to ensure the killer gets the credit. */
  if f != c then
    call UnitDamageTargetBJ(f, c, dmg, ATTACK_TYPE_CHAOS, DAMAGE_TYPE_NORMAL)
  endif
  if IsUnitDeadBJ(t) or IsUnitDeadBJ(c) then
    call SVSDestroyEffectByIndex(i)
  else
    if (GetUnitState(t, UNIT_STATE_LIFE) - dmg - 1.0) <= SVSTargetMinLife[i] then
      call SetUnitState(t, UNIT_STATE_LIFE, SVSTargetMinLife[i])
    endif
  endif

  if not IsUnitHidden(f) and not IsUnitHidden(t) then
    set ex = tx + (fx - tx)/distance*50.0
    set ey = ty + (fy - ty)/distance*50.0
    set e = AddSpecialEffect(VOID_SHIELD_EFFECT_IMPACT, ex, ey)
    call DestroyEffect(e)
  endif

  set e = null
  set trg = null
  set f = null
  set t = null
endfunction

function SVSTimerDurationCallback takes nothing returns nothing
  local timer t = GetExpiredTimer()
  local integer i = 0

  loop
    exitwhen i >= SVSQuantity
    if t == SVSTimerDuration[i] then
      call SVSDestroyEffectByIndex(i)
    endif
    set i = i + 1
  endloop

  call DestroyTimer(t)
  set t = null
endfunction

function SVSOnCastFilter takes nothing returns boolean
  return VOID_SHIELD == GetSpellAbilityId() and GetSpellTargetUnit() != null
endfunction

function SVSOnCastCallback takes nothing returns nothing
  local trigger trg = null
  local unit c = GetSpellAbilityUnit()
  local unit t = GetSpellTargetUnit()
  local integer i = 0 
  
  if SVSQuantity >= JASS_MAX_ARRAY_SIZE then
    return
  endif
  set i = SVSQuantity

  call ShowUnit(c, false)
  
  set trg = CreateTrigger()
  /*set trg = SVSDamageTrigger*/
  call TriggerRegisterUnitEvent(trg, t, EVENT_UNIT_DAMAGED)
  call TriggerAddCondition(trg, Condition(function SVSOnDamageFilter))
  call TriggerAddAction(trg, function SVSOnDamageCallback)

  /* If the target is already under the effect of another void shield,
  then turn off the effect of this new void shield,
  until the older effect expires or is canceled. 
  See SVSDestroyEffectByIndex.*/
  if IsUnitInGroup(t, SVSTargetsGroup) then
    call DisableTrigger(trg)
  endif

  set SVSDamageTriggers[i] = trg
  set SVSCasters[i] = c
  set SVSTargetMinLife[i] = RMaxBJ(GetUnitState(t, UNIT_STATE_LIFE), 4.0)
  set SVSTargets[i] = t
  set SVSTimerDuration[i] = CreateTimer()
  call TimerStart(SVSTimerDuration[i], VOID_SHIELD_DURATION, false, function SVSTimerDurationCallback)
  call GroupAddUnit(SVSTargetsGroup, t)
  set SVSEffects[i] = AddSpecialEffectTarget(VOID_SHIELD_EFFECT_TARGET, t, "overhead")

  set SVSQuantity = SVSQuantity + 1

  set c = null
  set t = null
  set trg = null
endfunction

function InitSpellVoidShield takes nothing returns nothing
  local trigger t = CreateTrigger()
  local trigger t2 = null

  set SVSTargetsGroup = CreateGroup()
  set SVSDamageTrigger = CreateTrigger()

  call TriggerAddCondition(SVSDamageTrigger, Condition(function SVSOnDamageFilter))
  call TriggerAddAction(SVSDamageTrigger, function SVSOnDamageCallback)

  call TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_SPELL_EFFECT)
  call TriggerAddCondition(t, Condition(function SVSOnCastFilter))
  call TriggerAddAction(t, function SVSOnCastCallback)

  set t2 = CreateTrigger()
  call TriggerRegisterAnyUnitEventBJ(t2, EVENT_PLAYER_UNIT_DEATH)
  call TriggerAddCondition(t2, Condition(function SVSOnDeathFilter))
  call TriggerAddAction(t2, function SVSOnDeathCallback)

  set t = null
  set t2 = null
endfunction
/* End SpellVoidShield.j */
