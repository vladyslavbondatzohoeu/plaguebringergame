/* Begin Waves 
Version: 2.1 */
globals
    constant real WAVE_TIMER_TIMEOUT = 0.05
    constant real WAVE_SPEED_PER_TIMEOUT = 512.0*WAVE_TIMER_TIMEOUT
    constant real WAVE_FORCE_PER_TIMEOUT = WAVE_SPEED_PER_TIMEOUT*0.3
    constant real WAVE_DAMAGE_PER_TIMEOUT = 10.0*WAVE_TIMER_TIMEOUT
    constant real WAVE_SIZE = 5000.0
    constant real WAVE_DURATION = 6.0
    constant real WAVE_DIRECTION_X = 0.0
    constant real WAVE_DIRECTION_Y = 1.0
    constant real WAVE_DELAY = 60.0
    
    timer wavesTimer
    timer wavesDelayTimer
    timer wavesDurationTimer
    timerdialog wavesDelayTimerDialog
    group wavesWashedGroup
    group groupCoveredFromWaves
    boolexpr wavesWashedGroupFilter
    real waveX
    real waveY
    boolean isWashing 
      
    region safeRegion = CreateRegion()
    rect array safeRects
    integer safeRectsQuantity = 0
endglobals

function PingMinimapForWaves takes nothing returns nothing
    call PingMinimapEx(waveX, waveY, 1.0, 255, 255, 255, false)
endfunction

function IsPointWashed takes real x, real y returns boolean
    return SquareRoot((waveY - y)*(waveY - y)) <= waveY
endfunction

function WavesWashedGroupFilterCallback takes nothing returns boolean 
    local boolean result = false
    local unit u = GetFilterUnit()
    
    if USER != GetOwningPlayer(u) then
        set result = false
    elseif IsUnitInGroup(u, groupCoveredFromWaves) then
        set result = false
    elseif IsUnitType(u, UNIT_TYPE_FLYING) then
        set result = false
    elseif IsUnitInTransport(u, udg_ship) then
        set result = false
    else
        set result = true
    endif
    
    set u = null
    
    return result  
endfunction

function WavesWashedGroupCallback takes nothing returns nothing  
    local unit u = GetEnumUnit()
    //local effect e = null
    local real ux = GetUnitX(u)
    local real uy = GetUnitY(u)
    local real tx = ux
    local real ty = uy + WAVE_FORCE_PER_TIMEOUT 
    //local real ex = ux + GetRandomReal(-64, 64)
    //local real ey = uy + GetRandomReal(-64, 64)
         
    if not IsUnitType(u, UNIT_TYPE_STRUCTURE) then  
        call SetUnitPosition(u, tx, ty) 
    endif
    call SetUnitFacingTimed(u, GetRandomDirectionDeg(), WAVE_TIMER_TIMEOUT) 
    call UnitDamageTargetBJ(u, u, WAVE_DAMAGE_PER_TIMEOUT , ATTACK_TYPE_SIEGE, DAMAGE_TYPE_NORMAL)
    
    //set e = AddSpecialEffect("Objects\\Spawnmodels\\Naga\\NagaDeath\\NagaDeath.mdl", ex, ey)
    //call DestroyEffect(e)
    
    set u = null 
    //set e = null
endfunction 

function DestroyAllWaves takes nothing returns nothing    
    set isWashing = false
    call PauseTimer(wavesTimer)

    call CameraClearNoiseForPlayer(USER) 
    call SetTerrainFogExBJ(0, 0.00, 5000.00, 0, 20.00, 20.00, 20.00)
endfunction

function WavesTimerCallback takes nothing returns nothing
    local real cameraX = 0.0
    local real cameraY = 0.0
    local real dist = 0.0 
    local effect e = null     
    local real ex = 0.0
    local real ey = 0.0
    
    call GroupEnumUnitsOfPlayer(wavesWashedGroup, USER, wavesWashedGroupFilter)    
    call ForGroup(wavesWashedGroup, function WavesWashedGroupCallback)
    
    if USER == GetLocalPlayer() then   
      set cameraX = GetCameraTargetPositionX()
      set cameraY = GetCameraTargetPositionY()
      
      if not IsPointInRegion(safeRegion, cameraX, cameraY) then   
          set ex = cameraX + GetRandomReal(-1024.0, 1024.0)
          set ey = cameraY + GetRandomReal(-1024.0, 1024.0) 
          set e = AddSpecialEffect("Objects\\Spawnmodels\\Naga\\NagaDeath\\NagaDeath.mdl", ex, ey)
          call DestroyEffect(e)  
      endif
    endif

    set e = null
endfunction

function CreateAllWaves takes nothing returns nothing
    call DestroyAllWaves()
    set isWashing = true 
    call TimerStart(wavesTimer, WAVE_TIMER_TIMEOUT, true, function WavesTimerCallback)
    call TimerStart(wavesDurationTimer, WAVE_DURATION, false, function DestroyAllWaves)
    call CameraSetSourceNoiseForPlayer(USER, 16.00, 0.1)
    call SetTerrainFogExBJ(0, 0.00, 3000.00, 0, 0.00, 0.00, 20.00)
endfunction

function OnEnteringSafeRegionCallback takes nothing returns nothing
    call GroupAddUnit(groupCoveredFromWaves, GetEnteringUnit())   
endfunction

function OnLeavingSafeRegionCallback takes nothing returns nothing
    call GroupRemoveUnit(groupCoveredFromWaves, GetLeavingUnit())   
endfunction

function InitWaves takes nothing returns nothing
    local integer i = 0
    local trigger trg = null
    local group g = null
  
    set groupCoveredFromWaves = CreateGroup()
  
    set safeRects[0] = gg_rct_WavesSafetyRect000
    set safeRects[1] = gg_rct_WavesSafetyRect001
    set safeRects[2] = gg_rct_WavesSafetyRect002
    set safeRects[3] = gg_rct_WavesSafetyRect003
    set safeRects[4] = gg_rct_WavesSafetyRect004
    set safeRects[5] = gg_rct_WavesSafetyRect005
    set safeRects[6] = gg_rct_WavesSafetyRect006
    set safeRects[7] = gg_rct_WavesSafetyRect007
    set safeRects[8] = gg_rct_WavesSafetyRect008
    set safeRects[9] = gg_rct_WavesSafetyRect009
    set safeRects[10] = gg_rct_WavesSafetyRect010
    set safeRects[11] = gg_rct_WavesSafetyRect011
    set safeRects[12] = gg_rct_WavesSafetyRect012
    set safeRects[13] = gg_rct_WavesSafetyRect013
    set safeRects[14] = gg_rct_WavesSafetyRect014
    set safeRectsQuantity = 15
  
    set g = CreateGroup()
    set safeRegion = CreateRegion()
    loop
      exitwhen i >= safeRectsQuantity
      call RegionAddRect(safeRegion, safeRects[i])
      call GroupEnumUnitsInRect(g, safeRects[i], null)
      call GroupAddGroup(g, groupCoveredFromWaves)
      set i = i + 1
    endloop
    call DestroyGroup(g)
    set g = null
  
    set trg = CreateTrigger()
    call TriggerRegisterEnterRegion(trg, safeRegion, null)
    call TriggerAddAction(trg, function OnEnteringSafeRegionCallback)
    set trg = null 
  
    set trg = CreateTrigger()
    call TriggerRegisterLeaveRegion(trg, safeRegion, null)
    call TriggerAddAction(trg, function OnLeavingSafeRegionCallback)
    set trg = null
  
    set waveX = -21000.0
    set waveY = -21000.0
    set isWashing = false
    set wavesWashedGroup = CreateGroup()
    set wavesWashedGroupFilter = Condition(function WavesWashedGroupFilterCallback)   
    set wavesTimer = CreateTimer()  
    set wavesDurationTimer = CreateTimer()
    set wavesDelayTimer = CreateTimer() 
    set wavesDelayTimerDialog = CreateTimerDialog(wavesDelayTimer)
    call TimerDialogSetTitle(wavesDelayTimerDialog, "Wave")
endfunction
/* End Waves */
