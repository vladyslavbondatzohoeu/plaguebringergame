/* SpellCorpseGas.j 1.1 */
globals
  constant integer CORPSE_GAS = 'A600'

  constant real SCG_DELAY = 5.0
  constant real SCG_UPDATE_TIMEOUT = 0.1
  constant string SCGEffectExplosion = "war3campImported\\assets\\Soul_Sorcery\\Soul_Blast.mdl"
  constant string SCGEffectImpact = "war3campImported\\assets\\Recolored_Mana_Burn\\Mana_Burn_Soul.mdl" 
  constant string SCGEffectIndicator = "war3campImported\\assets\\Soul_Sorcery\\Soul_Curse.mdx"
  effect array SCGEffects
  group SCGVictims
  integer SCGTargetsQuantity
  integer array SCGLevel
  real array SCGDuration
  texttag array SCGIndicators
  unit array SCGCasters
  unit array SCGTargets
endglobals

function spellCorpseGas takes unit c, unit t, integer lvl returns nothing
  local integer i = SCGTargetsQuantity

  if (i >= JASS_MAX_ARRAY_SIZE) or (null == c) or (null == t) or (lvl < 1) then
    return
  endif


  set SCGCasters[i] = c
  set SCGDuration[i] = SCG_DELAY
  set SCGEffects[i] = AddSpecialEffectTarget(SCGEffectIndicator, t, "origin")
  set SCGIndicators[i] = CreateTextTagUnitBJ(I2S(R2I(SCG_DELAY)), t, 0.0, 8.0, 100.0, 100.0, 100.0, 0)
  set SCGLevel[i] = lvl
  set SCGTargets[i] = t

  set SCGTargetsQuantity = SCGTargetsQuantity + 1
endfunction

function SCGDestructibleCallback takes nothing returns nothing
    call SetDestructableLife( GetEnumDestructable(), ( GetDestructableLife(GetEnumDestructable()) - 50.00 ) )
endfunction

function SCGOnCastCallback takes nothing returns nothing
  local unit caster = GetSpellAbilityUnit()

  local integer spellLevel = GetUnitAbilityLevel(caster, CORPSE_GAS)

  call spellCorpseGas(caster, GetSpellTargetUnit(), spellLevel)

  set caster = null
endfunction

function SCGUpdateCallback takes nothing returns nothing
  local boolean mustBeDestroyed = false
  local boolean isVictimAffected = false
  local integer i = SCGTargetsQuantity
  local real dmg = 0.0
  local texttag tt = null
  local texttag tt2 = null
  local unit f = null
  local unit u = null
  local location loc = null
  local integer victimsQuantity = 0

  loop
    exitwhen i <= 0
    set i = i - 1
    set u = SCGTargets[i]
    if IsUnitAliveBJ(u) then
      /* track duration */
      set SCGDuration[i] = SCGDuration[i] - SCG_UPDATE_TIMEOUT
      if SCGDuration[i] <= 2.0 then
        call SetTextTagColorBJ(tt, 100.0, 0.0, 0.0, 0.0)
      endif
      /* end track duration */

      /* update visuals */
      set tt = SCGIndicators[i]
      call SetTextTagTextBJ(tt, I2S(R2I(SCGDuration[i])), 8.0)
      call SetTextTagPosUnitBJ(tt, u, 0.0)
      /* end update visuals */

      /* explode when needed */
      if SCGDuration[i] <= 0.0 then
	call GroupClear(SCGVictims)
	call GroupEnumUnitsInRangeCounted(SCGVictims, GetUnitX(u), GetUnitY(u), 512.0, null, 6)
	set dmg = (30.0 + 30.0 * I2R(SCGLevel[i])) * (GetUnitState(u, UNIT_STATE_LIFE) / 300.0)
	loop
	  set f = FirstOfGroup(SCGVictims)
	  exitwhen null == f
	  set isVictimAffected = not IsUnitType(f, UNIT_TYPE_UNDEAD) and IsUnitAliveBJ(f) and GetUnitAbilityLevel(f, 'Avul') == 0
	  if isVictimAffected then
	    call UnitDamageTargetBJ(SCGCasters[i], f, dmg, ATTACK_TYPE_MAGIC, DAMAGE_TYPE_DISEASE)
	    call DestroyEffect(AddSpecialEffectTarget(SCGEffectImpact, f, "origin"))
	    set victimsQuantity = victimsQuantity + 1
	  endif
	  call GroupRemoveUnit(SCGVictims, f)
	endloop

        set loc = GetUnitLoc(u)
        call EnumDestructablesInCircleBJ(256.0, loc, function SCGDestructibleCallback)

        /* indicate damage */
        set tt2 = CreateTextTagLocBJ(I2S(R2I(dmg) * victimsQuantity), loc, 8.00, 10.00, 100, 34.00, 0.00, 0 )
        call SetTextTagPermanentBJ(tt2, false)
        call SetTextTagFadepointBJ(tt2, 1.00)
        call SetTextTagLifespanBJ(tt2, 2.00)
        /* end indicate damage */

	call DestroyEffect(AddSpecialEffectLoc(SCGEffectExplosion, loc))
	call RemoveLocation(loc)

        call SetUnitExploded(u, true)
	
        call UnitDamageTargetBJ(SCGCasters[i], u, 300.0, ATTACK_TYPE_MAGIC, DAMAGE_TYPE_DISEASE)
	call SetUnitExploded(u, false)
      endif
      /* end explode if needed */
    endif

    /* cleanup */
    set mustBeDestroyed = IsUnitDeadBJ(u) or IsUnitHidden(u) or SCGDuration[i] <= 0.0
    if mustBeDestroyed then
      call DestroyEffect(SCGEffects[i])
      call DestroyTextTag(SCGIndicators[i])

      set SCGTargetsQuantity = SCGTargetsQuantity - 1

      set SCGCasters[i] = SCGCasters[SCGTargetsQuantity]
      set SCGDuration[i] = SCGDuration[SCGTargetsQuantity]
      set SCGEffects[i] = SCGEffects[SCGTargetsQuantity]
      set SCGIndicators[i] = SCGIndicators[SCGTargetsQuantity]
      set SCGLevel[i] = SCGLevel[SCGTargetsQuantity]
      set SCGTargets[i] = SCGTargets[SCGTargetsQuantity]

      set SCGCasters[SCGTargetsQuantity] = null
      set SCGEffects[SCGTargetsQuantity] = null
      set SCGIndicators[SCGTargetsQuantity] = null
      set SCGTargets[SCGTargetsQuantity] = null
    endif
    /* end cleanup */
  endloop

  set f = null
  set tt = null
  set tt2 = null 
  set u = null
  set loc = null
endfunction

function SCGOnCastFilter takes nothing returns boolean
  local unit c = GetSpellAbilityUnit()
  local unit t = GetSpellTargetUnit()
  local boolean result = true

  set result = CORPSE_GAS == GetSpellAbilityId() and result
  set result = t != null and result
  set result = IsUnitType(t, UNIT_TYPE_UNDEAD) and result
  set result = GetOwningPlayer(t) == GetOwningPlayer(c) and result
  return result
endfunction

function InitSpellCorpseGas takes nothing returns nothing
  local trigger t = CreateTrigger()

  set SCGTargetsQuantity = 0
  set SCGVictims = CreateGroup()

  call TimerStart(CreateTimer(), SCG_UPDATE_TIMEOUT, true, function SCGUpdateCallback)

  call TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_SPELL_EFFECT)
  call TriggerAddCondition(t, Condition(function SCGOnCastFilter))
  call TriggerAddAction(t, function SCGOnCastCallback)

  set t = null
endfunction
/* End SpellCorpseGas.j */
