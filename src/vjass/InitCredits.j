function InitCredits takes nothing returns nothing
    call Credit("Author", "Zahanc", "Story, level-design, programming, assets. https://xgm.guru/user/Zahanc https://www.hiveworkshop.com/members/hetgrum.262222/")
    call Credit("Music", "Eliphas", "https://www.youtube.com/channel/UCu-gp8eEmEz4bDt2Q5pXVbQ")
    call Credit("Assets", "JetFangInferno", "Void Shield spell effect")
    call Credit("Assets", "JEDI_Knight", "Undead ships")
    call Credit("Assets", "Empyreal", "Corpse gas spell effects")
endfunction
