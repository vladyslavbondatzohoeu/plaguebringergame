// Begin DifficultyDialog.j
// Version: 1.0
globals
    trigger ddBeginGameTrigger = null
endglobals

function DDOnRelaxedCallback takes nothing returns nothing
    call SetGameDifficulty(MAP_DIFFICULTY_EASY)
    call ConditionalTriggerExecute(ddBeginGameTrigger)
endfunction

function DDOnFairCallback takes nothing returns nothing
    call SetGameDifficulty(MAP_DIFFICULTY_NORMAL)
    call ConditionalTriggerExecute(ddBeginGameTrigger)
endfunction

function DDOnMadCallback takes nothing returns nothing
    call SetGameDifficulty(MAP_DIFFICULTY_HARD)
    call ConditionalTriggerExecute(ddBeginGameTrigger)
endfunction

function InitDifficultyDialog takes trigger beginGameTrigger returns nothing        
    local dialog difficultyDialog = null
    local button difficultyButtonRelaxed = null
    local button difficultyButtonFair = null
    local button difficultyButtonMad = null
    local trigger t0 = null
    local trigger t1 = null
    local trigger t2 = null

    set ddBeginGameTrigger = beginGameTrigger
    set difficultyDialog = DialogCreate()
    call DialogClear(difficultyDialog)
    call DialogSetMessage(difficultyDialog, "Difficulty level selection")

    set difficultyButtonRelaxed = DialogAddButton(difficultyDialog, "|cffffcc00R|r|cffffffffelaxed|r", 'R') 
    set difficultyButtonFair = DialogAddButton(difficultyDialog, "|cffffcc00F|r|cffffffffair|r", 'F') 
    set difficultyButtonMad = DialogAddButton(difficultyDialog, "|cffffffffM|r|cffffcc00a|r|cffffffffd|r", 'A') 
    
    set t0 = CreateTrigger()
    set t1 = CreateTrigger()
    set t2 = CreateTrigger()
    call TriggerRegisterDialogButtonEvent(t0, difficultyButtonRelaxed)
    call TriggerRegisterDialogButtonEvent(t1, difficultyButtonFair)
    call TriggerRegisterDialogButtonEvent(t2, difficultyButtonMad)
    call TriggerAddAction(t0, function DDOnRelaxedCallback)
    call TriggerAddAction(t1, function DDOnFairCallback)
    call TriggerAddAction(t2, function DDOnMadCallback)
    
    call DialogDisplay(USER, difficultyDialog, true)
    
    set difficultyDialog = null
    set difficultyButtonRelaxed = null
    set difficultyButtonFair = null
    set difficultyButtonMad = null

    set t0 = null
    set t1 = null
    set t2 = null
endfunction
// End DifficultyDialog.j
