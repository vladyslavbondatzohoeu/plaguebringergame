globals
  constant player USER = Player(0)

  timer wavesTimer
  group washedUnits
  group iteratedWashedUnits
  boolexpr washedUnitsFilter
  
  region safeRegion
  rect array safeRects
  integer safeRectsQuantity = 0

  integer wavesQuantity
  real array waveX
  real array waveY
  real array waveXD
  real array waveYD
  real array waveEffectDuration
  boolean array waveAlive

  real array waveTargetX
  real array waveTargetY
  
  real array directionsX
  real array directionsY
  real array directionsTargetX
  real array directionsTargetY
  boolean array directionsAlive
  integer directionsQuantity

  integer waveIterator
  location dummyLoc
  
  group groupCoveredFromWaves

  constant real WAVE_RADIUS = 128.0
  constant real WAVE_TIMER_TIMEOUT_SECONDS = 0.04
  constant real WAVE_DAMAGE_PER_TIMEOUT = 1.0
  constant real WAVE_SPEED = 512.0
endglobals

function PingMinimapForWaves takes nothing returns nothing
  local integer i = 0

  loop
    exitwhen i >= directionsQuantity
    if directionsAlive[i] then
      call PingMinimapEx(directionsX[i], directionsY[i], 1.0, 255, 255, 255, false)
    endif
    set i = i + 1
  endloop
endfunction

function IsPointWashed takes real x, real y returns boolean
  local integer i = 0
  
  loop
    exitwhen i >= directionsQuantity     
    // Heuristics.
    if SquareRoot((directionsY[i] - y)*(directionsY[i] - y)) < WAVE_RADIUS + 16.0 then
      return true
    endif
    set i = i + 1
  endloop
  
  return false
endfunction

function washedUnitsFilterCallback takes nothing returns boolean
  local boolean result = false
  local unit u = GetFilterUnit()
  local real x = GetUnitX(u)
  local real y = GetUnitY(u)
  
  if USER != GetOwningPlayer(u) then
    set result = false
  elseif IsUnitType(u, UNIT_TYPE_FLYING) then   
    set result = false
  //elseif IsPointInRegion(safeRegion, x, y) then    
  elseif IsUnitInGroup(u, groupCoveredFromWaves) then  
    set result = false
  elseif not IsPointWashed(x, y) then
    set result = false
  else 
    set result = true
  endif
  
  set u = null
  
  return result
endfunction

function washedUnitsCallback takes nothing returns nothing
  local unit u = GetEnumUnit()
  if not IsUnitType(u, UNIT_TYPE_STRUCTURE) then      
      // Heuristics.
    call MoveLocation(dummyLoc, GetUnitX(u), GetUnitY(u) + WAVE_SPEED*WAVE_TIMER_TIMEOUT_SECONDS)
    call SetUnitPositionLoc(u, dummyLoc)
  endif      
  call UnitDamageTargetBJ(u, u, WAVE_DAMAGE_PER_TIMEOUT , ATTACK_TYPE_SIEGE, DAMAGE_TYPE_NORMAL )
  set u = null
endfunction

function wavesTimerCallback takes nothing returns nothing
  local integer i = 0
  local integer j = 0
                
  call GroupEnumUnitsOfPlayer(washedUnits, USER, washedUnitsFilter)    
  call ForGroup(washedUnits, function washedUnitsCallback)
  
  loop
    exitwhen j >= directionsQuantity
    if directionsAlive[j] then
      // Heuristics.
      set directionsX[j] = directionsX[j]// + WAVE_SPEED*WAVE_TIMER_TIMEOUT_SECONDS
      set directionsY[j] = directionsY[j] + WAVE_SPEED*WAVE_TIMER_TIMEOUT_SECONDS
                  
      if SquareRoot((directionsTargetX[j]-directionsX[j])*(directionsTargetX[j]-directionsX[j])+(directionsTargetY[j]-directionsY[j])*(directionsTargetY[j]-directionsY[j])) < WAVE_RADIUS then
        set directionsAlive[j] = false         
      endif  
    endif
                                                                            
    set j = j + 1
  endloop
  
  loop
    exitwhen i >= wavesQuantity
    if waveAlive[i] then 

      if waveEffectDuration[i] > 1.0 then
        set waveEffectDuration[i] = 0
        //if not IsPointInRegion(safeRegion, waveX[i], waveY[i]) then
          call DestroyEffect(AddSpecialEffect("Objects\\Spawnmodels\\Naga\\NagaDeath\\NagaDeath.mdl", waveX[i] + GetRandomReal(-32, 32), waveY[i] + GetRandomReal(-32, 32)))
        //endif
      endif
      
      //set waveAlive[i] = not IsTerrainPathable(waveX[i], waveY[i], PATHING_TYPE_AMPHIBIOUSPATHING) and waveAlive[i]
      if SquareRoot((waveTargetX[i]-waveX[i])*(waveTargetX[i]-waveX[i])+(waveTargetY[i]-waveY[i])*(waveTargetY[i]-waveY[i])) < WAVE_RADIUS then
        set waveAlive[i] = false
      endif   
      
      set waveEffectDuration[i] = waveEffectDuration[i] + WAVE_TIMER_TIMEOUT_SECONDS
      set waveX[i] = waveX[i] + waveXD[i]
      set waveY[i] = waveY[i] + waveYD[i]
    endif
    set i = i + 1
  endloop
endfunction

function CreateWaves takes real x0, real y0, real x1, real y1, integer size returns nothing
  local real ax = x1 - x0
  local real ay = y1 - y0
  local real am = SquareRoot(ax*ax + ay*ay)
  local real aux = ax/am
  local real auy = ay/am

  local real bux = -auy
  local real buy = aux
  local real bx = bux*I2R(size)*WAVE_RADIUS 
  local real by = buy*I2R(size)*WAVE_RADIUS
  local real bm = SquareRoot(bx*bx + by*by)

  local integer i = 0
  local integer j = 0
  local integer k = 0   
  
  loop
    exitwhen k >= directionsQuantity
    if not directionsAlive[k] then
      set directionsQuantity = directionsQuantity - 1 
  
      set directionsX[k] = directionsX[directionsQuantity] 
      set directionsY[k] = directionsY[directionsQuantity]  
      set directionsTargetX[k] = directionsTargetX[directionsQuantity] 
      set directionsTargetY[k] = directionsTargetY[directionsQuantity] 
      set directionsAlive[k] = directionsAlive[directionsQuantity]
  
      set directionsX[directionsQuantity] = 0.0
      set directionsY[directionsQuantity] = 0.0   
      set directionsTargetX[directionsQuantity] = 0.0
      set directionsTargetY[directionsQuantity] = 0.0
      set directionsAlive[directionsQuantity] = false
    endif
    set k = k + 1
  endloop
           
  set directionsX[directionsQuantity] = x0
  set directionsY[directionsQuantity] = y0    
  set directionsTargetX[directionsQuantity] = x1
  set directionsTargetY[directionsQuantity] = y1
  set directionsAlive[directionsQuantity] = true

  loop
    exitwhen j >= wavesQuantity
    if not waveAlive[j] then
      set wavesQuantity = wavesQuantity - 1

      set waveX[j] = waveX[wavesQuantity]
      set waveY[j] = waveY[wavesQuantity]
      set waveTargetX[j] = waveTargetX[wavesQuantity]
      set waveTargetY[j] = waveTargetY[wavesQuantity]
      set waveXD[j] = waveXD[wavesQuantity]
      set waveYD[j] = waveYD[wavesQuantity]
      set waveAlive[j] = waveAlive[wavesQuantity]

      set waveX[wavesQuantity] = 0.0
      set waveY[wavesQuantity] = 0.0
      set waveTargetX[wavesQuantity] = 0.0
      set waveTargetY[wavesQuantity] = 0.0
      set waveXD[wavesQuantity] = 0.0
      set waveYD[wavesQuantity] = 0.0
      set waveAlive[wavesQuantity] = false
    endif
    set j = j + 1
  endloop
            
  loop
    exitwhen i >= size
    set waveX[wavesQuantity + i] = x0 + bux*WAVE_RADIUS*I2R(i) - bx/2.0
    set waveY[wavesQuantity + i] = y0 + buy*WAVE_RADIUS*I2R(i) - by/2.0
    set waveTargetX[wavesQuantity + i] = waveX[wavesQuantity + i] + ax
    set waveTargetY[wavesQuantity + i] = waveY[wavesQuantity + i] + ay
    set waveXD[wavesQuantity + i] = aux*WAVE_SPEED*WAVE_TIMER_TIMEOUT_SECONDS
    set waveYD[wavesQuantity + i] = auy*WAVE_SPEED*WAVE_TIMER_TIMEOUT_SECONDS
    set waveAlive[wavesQuantity + i] = true
    set i = i + 1
  endloop 
  set wavesQuantity = wavesQuantity + size    
  set directionsQuantity = directionsQuantity + 1

  call PingMinimapForWaves()     
endfunction

function CreateAllWaves takes nothing returns nothing
  local integer i = 0
  
  loop
    exitwhen i >= 3  
    call CreateWaves(0.0, -20000.0 + I2R(i)*WAVE_RADIUS*4.0, 0.0, -7500, 210)
    call CreateWaves(0.0, -7500 + I2R(i)*WAVE_RADIUS*4.0, 0.0, 500.0, 210)
    call CreateWaves(0.0, 500.0 + I2R(i)*WAVE_RADIUS*4.0, 0.0, 13000.0, 210)
    set i = i + 1
  endloop
  //call CreateWaves(0.0, -20000.0, 0.0, 13000.0, 210)
endfunction

function OnEnteringSafeRegionCallback takes nothing returns nothing
  call GroupAddUnit(groupCoveredFromWaves, GetEnteringUnit())   
endfunction

function OnLeavingSafeRegionCallback takes nothing returns nothing
  call GroupRemoveUnit(groupCoveredFromWaves, GetLeavingUnit())   
endfunction

function InitWaves takes nothing returns nothing
  local integer i = 0
  local trigger trg = null
  local boolean periodic = true
  local group g = null
  
  set groupCoveredFromWaves = CreateGroup()
  
  set safeRects[0] = gg_rct_WavesSafetyRect000
  set safeRects[1] = gg_rct_WavesSafetyRect001
  set safeRects[2] = gg_rct_WavesSafetyRect002
  set safeRects[3] = gg_rct_WavesSafetyRect003
  set safeRects[4] = gg_rct_WavesSafetyRect004
  set safeRects[5] = gg_rct_WavesSafetyRect005
  set safeRects[6] = gg_rct_WavesSafetyRect006
  set safeRects[7] = gg_rct_WavesSafetyRect007
  set safeRects[8] = gg_rct_WavesSafetyRect008
  set safeRects[9] = gg_rct_WavesSafetyRect009
  set safeRects[10] = gg_rct_WavesSafetyRect010
  set safeRectsQuantity = 11
  
  set g = CreateGroup()
  set safeRegion = CreateRegion()
  loop
    exitwhen i >= safeRectsQuantity
    call RegionAddRect(safeRegion, safeRects[i])
    call GroupEnumUnitsInRect(g, safeRects[i], null)
    call GroupAddGroup(g, groupCoveredFromWaves)
    set i = i + 1
  endloop
  call DestroyGroup(g)
  set g = null
  
  set trg = CreateTrigger()
  call TriggerRegisterEnterRegion(trg, safeRegion, null)
  call TriggerAddAction(trg, function OnEnteringSafeRegionCallback)
  set trg = null 
  
  set trg = CreateTrigger()
  call TriggerRegisterLeaveRegion(trg, safeRegion, null)
  call TriggerAddAction(trg, function OnLeavingSafeRegionCallback)
  set trg = null

  set wavesQuantity = 0
  set directionsQuantity = 0
  set dummyLoc = Location(0, 0)
  set washedUnits = CreateGroup()
  set iteratedWashedUnits = CreateGroup()
  set washedUnitsFilter = Condition(function washedUnitsFilterCallback)
  set wavesTimer = CreateTimer()
  call TimerStart(wavesTimer, WAVE_TIMER_TIMEOUT_SECONDS, periodic, function wavesTimerCallback)
endfunction
