/* Begin SpellExtinguishLife.j
Version: 1.0 */
globals
    constant integer EXTINGUISH_LIFE = 'A605'

    constant string SEL_EFFECT_CASTER = "Abilities\\Spells\\Items\\AIma\\AImaTarget.mdl"
    constant string SEL_EFFECT_IMPACT = "war3campImported\\assets\\Recolored_Mana_Burn\\Mana_Burn_Unholy.mdl"

    boolexpr SELUndeadGroupFilter = null
    player SELLastOwner = null
endglobals

function SELOnCastFinishFilter takes nothing returns boolean
    return EXTINGUISH_LIFE == GetSpellAbilityId() and IsUnitDeadBJ(GetSpellTargetUnit())
endfunction

function SELOnCastFinishCallback takes nothing returns nothing
    local effect e = null 
    local unit caster = GetSpellAbilityUnit()
    local real possibleMana = 50.0 + 50.0*I2R(GetUnitAbilityLevel(caster, GetSpellAbilityId()))
    call SetUnitState(caster, UNIT_STATE_MANA, GetUnitState(caster, UNIT_STATE_MANA) + possibleMana)
    set e = AddSpecialEffectTarget(SEL_EFFECT_CASTER, caster, "origin")
    call DestroyEffect(e)
    set e = null
    set caster = null
endfunction

function SELUndeadGroupFilterCallback takes nothing returns boolean
    local unit f = GetFilterUnit()
    local boolean result = false

    set result = SELLastOwner == GetOwningPlayer(f) and IsUnitType(f, UNIT_TYPE_UNDEAD)

    set f = null
    return result
endfunction

function CreateSpellEffectExtinguishLife takes unit caster, integer spellRank, unit target, real damage returns nothing
    local effect e = null
    local group g = CreateGroup()
    local player owner = GetOwningPlayer(caster)
    local real totalDamage = damage
    local real bonusDamage = 0.0
    local unit f = null

    call GroupEnumUnitsInRange(g, GetUnitX(target), GetUnitY(target), 512.0, null)
    loop
        set f = FirstOfGroup(g)
        exitwhen null == f 

        if owner == GetOwningPlayer(f) and IsUnitType(f, UNIT_TYPE_UNDEAD) and IsUnitAliveBJ(f) then
	    set bonusDamage = bonusDamage + 16.0

       	    set e = AddSpecialEffectTarget(SEL_EFFECT_IMPACT, f, "origin")
            call DestroyEffect(e)
	endif

	call GroupRemoveUnit(g, f)
    endloop
    set e = null
    set owner = null

    call DestroyGroup(g)
    set g = null

    set totalDamage = damage + RMaxBJ(bonusDamage, I2R(100*spellRank))
    call UnitDamageTargetBJ(caster, target, totalDamage, ATTACK_TYPE_MAGIC, DAMAGE_TYPE_NORMAL)
endfunction

function SELOnCastFilter takes nothing returns boolean
    return (EXTINGUISH_LIFE == GetSpellAbilityId()) and IsUnitAliveBJ(GetSpellTargetUnit())
endfunction

function SELOnCastCallback takes nothing returns nothing
    local unit caster = GetSpellAbilityUnit()
    local real damageAmount = 100.0
    call CreateSpellEffectExtinguishLife(caster, GetUnitAbilityLevel(caster, GetSpellAbilityId()), GetSpellTargetUnit(), damageAmount)
    set caster = null
endfunction

function InitSpellExtinguishLife takes nothing returns nothing
    local trigger t0 = null
    local trigger t1 = null

    set SELUndeadGroupFilter = Condition(function SELUndeadGroupFilterCallback)

    set t0 = CreateTrigger()
    call TriggerRegisterAnyUnitEventBJ(t0, EVENT_PLAYER_UNIT_SPELL_EFFECT)
    call TriggerAddCondition(t0, Condition(function SELOnCastFilter))
    call TriggerAddAction(t0, function SELOnCastCallback)
    
    set t1 = CreateTrigger()
    call TriggerRegisterAnyUnitEventBJ(t1, EVENT_PLAYER_UNIT_SPELL_FINISH)
    call TriggerAddCondition(t1, Condition(function SELOnCastFinishFilter))
    call TriggerAddAction(t1, function SELOnCastFinishCallback)

    set t0 = null
    set t1 = null
endfunction
/* End SpellExtinguishLife.j */
