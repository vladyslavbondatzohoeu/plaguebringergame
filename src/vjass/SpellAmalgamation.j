/* Begin SpellAmalgamation.j
Version: 1.0 */
globals
    boolexpr SACorpseGroupFilter = null
    constant integer AMALGAMATION = 'n605' 
    constant integer SA_CACHE_SIZE_MAX = 32
    constant integer SA_LIFE_BONUS_1 = 'A607' 
    constant integer SA_LIFE_BONUS_2 = 'A609' 
    constant integer SA_LIFE_BONUS_3 = 'A60A' 
    constant integer SA_LIFE_BONUS_4 = 'A60B' 
    constant integer SA_LIFE_BONUS_5 = 'A60C' 
    constant integer SA_LIFE_BONUS_6 = 'A60D' 
    constant integer SA_LIFE_BONUS_7 = 'A60E' 
    constant integer SA_MANA_BONUS_1 = 'A60F'
    constant integer SA_MANA_BONUS_2 = 'A60G'
    constant integer SA_MANA_BONUS_3 = 'A60H'
    constant integer SA_MANA_BONUS_4 = 'A60I'
    constant integer SA_MANA_BONUS_5 = 'A60J'
    constant integer SA_MANA_BONUS_6 = 'A60K'
    constant integer SA_MANA_BONUS_7 = 'A608'
    constant integer SUMMON_AMALGAMATION = 'A606'
    integer SACacheSize = 0
    integer SAInheritableUnitTypeIdQuantity = 0 
    integer SAQuantity = 0
    integer array SACacheAbilityId
    integer array SACacheUnitTypeId
    integer array SAInheritableAbilityId
    integer array SAInheritableUnitTypeId
    unit array SACaster
    unit array SASummon
endglobals

function SAOnCastFilter takes nothing returns boolean
    return SUMMON_AMALGAMATION == GetSpellAbilityId()
endfunction

function SAGetInheritedAbilityId takes integer corpseUnitTypeId returns integer
    local boolean break = false
    local integer inheritedAbilityId = 0
    local integer i = 0
    local integer j = 0
    local boolean cached = false
   
    if corpseUnitTypeId > 0 then

         set i = 0
         loop
	     exitwhen i >= SACacheSize or break 
	     if corpseUnitTypeId == SACacheUnitTypeId[i] then
	         set inheritedAbilityId = SACacheAbilityId[i]
		 set break = true
		 set cached = true
	     endif
	     set i = i + 1
	 endloop

         set i = 0
         loop
             exitwhen i >= SAInheritableUnitTypeIdQuantity or break 
             if corpseUnitTypeId == SAInheritableUnitTypeId[i] then
	         set inheritedAbilityId = SAInheritableAbilityId[i]
                 set break = true
	     endif
	     set i = i + 1
         endloop

         if not cached then
	      if (SACacheSize + 1) >= SA_CACHE_SIZE_MAX then
	          set j = 0
	          loop
	              exitwhen j >= SACacheSize
		      set SACacheUnitTypeId[j] = SACacheUnitTypeId[j + 1]
	              set SACacheAbilityId[j] = SACacheAbilityId[j + 1]
		      set j = j + 1
	          endloop
	          set SACacheSize = SACacheSize - 1
	      endif
	      set SACacheUnitTypeId[SACacheSize] = corpseUnitTypeId
	      set SACacheAbilityId[SACacheSize] = inheritedAbilityId
	      set SACacheSize = SACacheSize + 1
         endif
    endif

    return inheritedAbilityId 
endfunction

function SARegisterUnitTypeId takes integer unitTypeId, integer abilityId returns nothing
    set SAInheritableUnitTypeId[SAInheritableUnitTypeIdQuantity] = unitTypeId
    set SAInheritableAbilityId[SAInheritableUnitTypeIdQuantity] = abilityId
    set SAInheritableUnitTypeIdQuantity = SAInheritableUnitTypeIdQuantity + 1
endfunction

function SACorpseGroupFilterCallback takes nothing returns boolean
    return IsUnitDeadBJ(GetFilterUnit()) and not IsUnitType(GetFilterUnit(), UNIT_TYPE_MECHANICAL)
endfunction

function CreateSpellEffectAmalgamation takes unit caster, integer summonUnitTypeId, real x, real y, real aoe returns nothing
    local group corpses = null 
    local integer amalgamationQuantity = SAQuantity
    local integer corpseUnitTypeId = 0
    local integer i = 0
    local integer j = amalgamationQuantity
    local unit amalgamation = null
    local unit corpse = null
    local integer casterCorpsesQuantity = 0
    local integer corpsesQuantity = 0
    local integer noncasterCorpsesQuantity = 0

    set aoe = RMinBJ(RMaxBJ(aoe, 32.0), 10000.0)

    if null == caster then
        return
    endif

    /* Ensure each caster can have only one amalgamation. */
    loop
        exitwhen i >= amalgamationQuantity
	if SACaster[i] == caster then
	    call KillUnit(SASummon[i])
	    set j = i
	endif
	set i = i + 1
    endloop
    /* End ensure. */

    set amalgamation = CreateUnit(GetOwningPlayer(caster), summonUnitTypeId, x, y, GetUnitFacing(caster))
    call SetUnitUseFood(amalgamation, false)
    call UnitAddType(amalgamation, UNIT_TYPE_SUMMONED)
    call UnitAddType(amalgamation, UNIT_TYPE_UNDEAD)
    call SetUnitExploded(amalgamation, true)

    /* Inherit abilities. */
    set corpses = CreateGroup()
    call GroupEnumUnitsInRange(corpses, x, y, aoe*0.5, SACorpseGroupFilter)
    loop
        set corpse = FirstOfGroup(corpses)
        exitwhen corpse == null
        set corpseUnitTypeId = GetUnitTypeId(corpse)

        call UnitAddAbility(amalgamation, SAGetInheritedAbilityId(corpseUnitTypeId))

	if GetUnitState(corpse, UNIT_STATE_MAX_MANA) >= 1.0 then
	    set casterCorpsesQuantity = casterCorpsesQuantity + 1
	endif

        set corpsesQuantity = corpsesQuantity + 1
	call GroupRemoveUnit(corpses, corpse)
	call RemoveUnit(corpse)
    endloop
    call DestroyGroup(corpses)
    set corpses = null

    set noncasterCorpsesQuantity = corpsesQuantity - casterCorpsesQuantity

    if 1 == casterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_MANA_BONUS_1)
    elseif 2 == casterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_MANA_BONUS_2)
    elseif 3 == casterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_MANA_BONUS_3)
    elseif 4 == casterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_MANA_BONUS_4)
    elseif 5 == casterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_MANA_BONUS_5)
    elseif 6 == casterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_MANA_BONUS_6)
    elseif casterCorpsesQuantity >= 7 then
        call UnitAddAbility(amalgamation, SA_MANA_BONUS_7)
    endif

    if 1 == noncasterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_LIFE_BONUS_1)
    elseif 2 == noncasterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_LIFE_BONUS_2)
    elseif 3 == noncasterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_LIFE_BONUS_3)
    elseif 4 == noncasterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_LIFE_BONUS_4)
    elseif 5 == noncasterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_LIFE_BONUS_5)
    elseif 6 == noncasterCorpsesQuantity then
        call UnitAddAbility(amalgamation, SA_LIFE_BONUS_6)
    elseif noncasterCorpsesQuantity >= 7 then
        call UnitAddAbility(amalgamation, SA_LIFE_BONUS_7)
    endif
    /* End inherit abilities. */

    set SACaster[j] = caster
    set SASummon[j] = amalgamation
    set amalgamation = null
    set SAQuantity = SAQuantity + 1
endfunction

function SAOnCastCallback takes nothing returns nothing
    local unit caster = GetSpellAbilityUnit()
    local real targetX = GetSpellTargetX()
    local real targetY = GetSpellTargetY()

    call CreateSpellEffectAmalgamation(caster, AMALGAMATION, targetX, targetY, 512.0)

    set caster = null
endfunction

function InitSpellAmalgamation takes nothing returns nothing
    local trigger t0 = null
 
    /* Banish */
    call SARegisterUnitTypeId('hspt', 'ACbn')
    call SARegisterUnitTypeId('ospw', 'ACbn')
    call SARegisterUnitTypeId('edot', 'ACbn')
   
    /* Chain Lightning */
    call SARegisterUnitTypeId('hsor', 'ACcl')
    call SARegisterUnitTypeId('oshm', 'ACcl')
    call SARegisterUnitTypeId('nltl', 'ACcl')
    call SARegisterUnitTypeId('nthl', 'ACcl')
    call SARegisterUnitTypeId('nstw', 'ACcl')
    
    /* Death Coil */
    call SARegisterUnitTypeId('unec', 'ACdc')
    call SARegisterUnitTypeId('nbld', 'ACdc')
    call SARegisterUnitTypeId('nsra', 'ACdc')
    call SARegisterUnitTypeId('nsrh', 'ACdc')
    call SARegisterUnitTypeId('nsrw', 'ACdc')
       
    /* Devour Magic */
    call SARegisterUnitTypeId('edry', 'ACde')
       
    call SARegisterUnitTypeId('npfl', 'ACde')
    call SARegisterUnitTypeId('nfel', 'ACde')
    call SARegisterUnitTypeId('npfm', 'ACde')

    /* Frenzy */
    call SARegisterUnitTypeId('ogru', 'Afzy')
    call SARegisterUnitTypeId('ugho', 'Afzy')
    call SARegisterUnitTypeId('edoc', 'Afzy')
       
    /* Frost Armor */
    call SARegisterUnitTypeId('nnsw', 'ACfa')
       
    call SARegisterUnitTypeId('nwiz', 'ACfa')
    call SARegisterUnitTypeId('nwzr', 'ACfa')
    call SARegisterUnitTypeId('nwzg', 'ACfa')

    /* Frost Nova */
    call SARegisterUnitTypeId('nnrg', 'ACfn')
    call SARegisterUnitTypeId('nwzd', 'ACfa')
       
    /* Healing Wave */
    call SARegisterUnitTypeId('hmpr', 'AChv')
    call SARegisterUnitTypeId('odoc', 'AChv')
    call SARegisterUnitTypeId('nfre', 'AChv')
    call SARegisterUnitTypeId('nmrv', 'AChv')
    call SARegisterUnitTypeId('ndtp', 'AChv')
    call SARegisterUnitTypeId('ndth', 'AChv')
    call SARegisterUnitTypeId('nrzm', 'AChv')
    call SARegisterUnitTypeId('nsqo', 'AChv')
       
    /* Hex */
    call SARegisterUnitTypeId('ohun', 'AChx')
    call SARegisterUnitTypeId('nsty', 'AChx')
    call SARegisterUnitTypeId('nsat', 'AChx')
    call SARegisterUnitTypeId('nsts', 'AChx')
    call SARegisterUnitTypeId('ndqn', 'AChx')
    call SARegisterUnitTypeId('ndqv', 'AChx')
    call SARegisterUnitTypeId('ndqt', 'AChx')
       
    /* Howl of Terror */
    call SARegisterUnitTypeId('nowb', 'Acht')
    call SARegisterUnitTypeId('nowe', 'Acht')
    call SARegisterUnitTypeId('nohk', 'Acht')
    call SARegisterUnitTypeId('nfgu', 'Acht')
    call SARegisterUnitTypeId('nfgb', 'Acht')
    call SARegisterUnitTypeId('nfov', 'Acht')
       
    /* Silence */
    call SARegisterUnitTypeId('ndqs', 'Acsi')
    call SARegisterUnitTypeId('ndqp', 'Acsi')
    call SARegisterUnitTypeId('nstl', 'Acsi')
    call SARegisterUnitTypeId('nsth', 'Acsi')
       
    /* Shockwave */
    call SARegisterUnitTypeId('hkni', 'ACsh')
    call SARegisterUnitTypeId('otau', 'ACsh')
    call SARegisterUnitTypeId('nbal', 'ACsh')
    call SARegisterUnitTypeId('ncks', 'ACsh')
    call SARegisterUnitTypeId('ncnk', 'ACsh')
    call SARegisterUnitTypeId('njgb', 'ACsh')
    call SARegisterUnitTypeId('njga', 'ACsh')
       
    /* Sleep */
    call SARegisterUnitTypeId('ndtr', 'ACsl')
    call SARegisterUnitTypeId('ndtt', 'ACsl')
    call SARegisterUnitTypeId('ndtb', 'ACsl')
    call SARegisterUnitTypeId('ndtw', 'ACsl')
    call SARegisterUnitTypeId('ndmu', 'ACsl')
    call SARegisterUnitTypeId('nzom', 'ACsl')
       
    /* Slam */
    call SARegisterUnitTypeId('okod', 'ACtc')
    call SARegisterUnitTypeId('uabo', 'ACtc')
    call SARegisterUnitTypeId('emtg', 'ACtc')
    call SARegisterUnitTypeId('nfra', 'ACtc')
    call SARegisterUnitTypeId('nsgb', 'ACtc')
    call SARegisterUnitTypeId('nogr', 'ACtc')
    call SARegisterUnitTypeId('nomg', 'ACtc')
    call SARegisterUnitTypeId('nogl', 'ACtc')
    call SARegisterUnitTypeId('nrzg', 'ACtc')
       
    /* Slow */
    call SARegisterUnitTypeId('nmcf', 'ACsw')
    call SARegisterUnitTypeId('nmdb', 'ACsw')
    call SARegisterUnitTypeId('nmtw', 'ACsw')
    call SARegisterUnitTypeId('nmsn', 'ACsw')
    call SARegisterUnitTypeId('nmsc', 'ACsw')
    call SARegisterUnitTypeId('nmrl', 'ACsw')
    call SARegisterUnitTypeId('nmrr', 'ACsw')
    call SARegisterUnitTypeId('nmpg', 'ACsw')
    call SARegisterUnitTypeId('nmfs', 'ACsw')
    call SARegisterUnitTypeId('nmrm', 'ACsw')
    call SARegisterUnitTypeId('nmmu', 'ACsw')
    call SARegisterUnitTypeId('opeo', 'ACsw')
    call SARegisterUnitTypeId('hpea', 'ACsw')
    call SARegisterUnitTypeId('nkob', 'ACsw')
    call SARegisterUnitTypeId('nkog', 'ACsw')
    call SARegisterUnitTypeId('nkol', 'ACsw')
    call SARegisterUnitTypeId('n601', 'ACsw')
    call SARegisterUnitTypeId('nkot', 'ACsw')
       
    /* Carrion Swarm */
    call SARegisterUnitTypeId('ucry', 'ACca')
    call SARegisterUnitTypeId('nnwq', 'ACca')
    call SARegisterUnitTypeId('nnws', 'ACca')

    /* Spiked Shell */
    call SARegisterUnitTypeId('hfoo', 'ANth')
    call SARegisterUnitTypeId('nhyc', 'ANth')
    call SARegisterUnitTypeId('ntrh', 'ANth')
    call SARegisterUnitTypeId('ntrs', 'ANth')
    call SARegisterUnitTypeId('ntrt', 'ANth')
    call SARegisterUnitTypeId('ntrg', 'ANth')
    call SARegisterUnitTypeId('ntrd', 'ANth')
    call SARegisterUnitTypeId('nspd', 'ANth')
    call SARegisterUnitTypeId('nnwa', 'ANth')
    call SARegisterUnitTypeId('nnwl', 'ANth')
    call SARegisterUnitTypeId('nnwr', 'ANth')
   
    set SACorpseGroupFilter = Condition(function SACorpseGroupFilterCallback)

    set t0 = CreateTrigger()

    call TriggerRegisterAnyUnitEventBJ(t0, EVENT_PLAYER_UNIT_SPELL_EFFECT)
    call TriggerAddCondition(t0, Condition(function SAOnCastFilter))
    call TriggerAddAction(t0, function SAOnCastCallback)

    set t0 = null
endfunction
/* End SpellAmalgamation.j */
