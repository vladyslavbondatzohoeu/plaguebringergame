/* SpellContaminate.j 1.1 */
globals
  constant integer CONTAMINATE = 'A601'
  constant integer CONTAMINATE_SUMMON_1 = 'n602'
  constant integer CONTAMINATE_SUMMON_2 = 'n603'
  constant integer CONTAMINATE_SUMMON_3 = 'n604'
  constant real CONTAMINATE_AOE = 200.0
  constant real CONTAMINATE_DURATION = 16.0
  constant real CONTAMINATE_SUMMON_DURATION = 60.0
  constant real CONTAMINATE_UPDATE_TIMER_TIMEOUT = 2.0

  constant string CONTAMINATE_EFFECT = "Abilities\\Spells\\Orc\\TrollBerserk\\TrollBeserkerTarget.mdl"
  constant string CONTAMINATE_EFFECT_SUMMON = "Abilities\\Spells\\Orc\\Devour\\DevourEffectArt.mdl"
 
  constant integer SC_MAX_EFFECTS_PER_INSTANCE = 16 

  integer SCQuantity = 0
  effect array SCEffect
  integer array SCLevel
  location array SCLoc
  player array SCOwner 
  timer array SCTimerExpiration
endglobals

function SCOnDeathFilter takes nothing returns boolean
  local boolean isConvertable = false
  local unit u = GetDyingUnit()
  set isConvertable = not IsUnitType(u, UNIT_TYPE_UNDEAD) and not IsUnitType(u, UNIT_TYPE_MECHANICAL)
  set u = null
  return isConvertable and (SCQuantity > 0)
endfunction

function SCOnDeathCallback takes nothing returns nothing
  local integer i = 0
  local unit f = null
  local unit u = GetTriggerUnit()
  local location loc = GetUnitLoc(u)
  local integer summonUnitTypeId = CONTAMINATE_SUMMON_1

  /* In case there are overlaping Contamination zones,
  make sure only one summon is created.
  Moreover, make sure only the oldest zone takes effect.*/
  loop
    exitwhen i >= SCQuantity or (f != null) 
    if DistanceBetweenPoints(loc, SCLoc[i]) <= CONTAMINATE_AOE then
      if SCLevel[i] >= 3 then
        set summonUnitTypeId = CONTAMINATE_SUMMON_3
      elseif SCLevel[i] == 2 then
        set summonUnitTypeId = CONTAMINATE_SUMMON_2
      else
        set summonUnitTypeId = CONTAMINATE_SUMMON_1
      endif
      set f = CreateUnitAtLoc(SCOwner[i], summonUnitTypeId, loc, GetUnitFacing(u))
      call SetUnitUseFood(f, false)
      call UnitAddType(f, UNIT_TYPE_SUMMONED)
      call UnitAddType(f, UNIT_TYPE_UNDEAD)
      call UnitApplyTimedLife(f, 'BTLF', CONTAMINATE_SUMMON_DURATION)
      call SetUnitExploded(f, true)

      call DestroyEffect(AddSpecialEffectLoc(CONTAMINATE_EFFECT_SUMMON, loc))
    endif
    set i = i + 1
  endloop

  call RemoveLocation(loc)
  set f = null
  set loc = null
  set u = null
endfunction

function SCOnCastFilter takes nothing returns boolean
  return CONTAMINATE == GetSpellAbilityId() 
endfunction

function SCTimerExpirationCallback takes nothing returns nothing
  local integer i = 0
  local timer t = GetExpiredTimer()
  local integer j = 0
  local integer k = 0
  loop
    exitwhen i >= SCQuantity
    if t == SCTimerExpiration[i] then
      call RemoveLocation(SCLoc[i])
      call DestroyTimer(SCTimerExpiration[i])
      set k = 0
      loop
        exitwhen k >= SC_MAX_EFFECTS_PER_INSTANCE
	call DestroyEffect(SCEffect[i*SC_MAX_EFFECTS_PER_INSTANCE + k])
	set k = k + 1
      endloop

      /* Pull other instances down the stack,
      to preserve the creation order,
      that is relevant in resolving application conflicts. */
      set j = i 
      loop
        exitwhen j >= SCQuantity
	set k = 0
	loop
	  exitwhen k >= SC_MAX_EFFECTS_PER_INSTANCE
	  set SCEffect[j*SC_MAX_EFFECTS_PER_INSTANCE + k] = SCEffect[(j + 1)*SC_MAX_EFFECTS_PER_INSTANCE + k]
	  set k = k + 1
	endloop
	set SCLevel[j] = SCLevel[j + 1]
	set SCLoc[j] = SCLoc[j + 1]
	set SCOwner[j] = SCOwner[j + 1]
	set SCTimerExpiration[j] = SCTimerExpiration[j + 1]
	set j = j + 1
      endloop
      set SCQuantity = SCQuantity - 1

      set t = null
      /* Assume each timer in the array is unique. */
      return
    endif
    set i = i + 1
  endloop
endfunction

function SCOnCastCallback takes nothing returns nothing
  local unit c = GetSpellAbilityUnit()
  local integer i = SCQuantity
  local location loc = null
  local location eLoc = null
  local real x = GetSpellTargetX()
  local real y = GetSpellTargetY()
  local integer spellRank = GetUnitAbilityLevel(c, GetSpellAbilityId())
  local real aoe = CONTAMINATE_AOE 
  local integer effectsQuantity = 0

  if i >= JASS_MAX_ARRAY_SIZE/SC_MAX_EFFECTS_PER_INSTANCE - 1 then
    return
  endif

  set loc = GetSpellTargetLoc()
  set eLoc = Location(x, y)

  call MoveLocation(eLoc, x + aoe/2.0*1.0, y + aoe/2.0*0.0)
  set SCEffect[i*SC_MAX_EFFECTS_PER_INSTANCE + 0] = AddSpecialEffectLoc(CONTAMINATE_EFFECT, eLoc)
  call MoveLocation(eLoc, x + aoe/2.0*0.7, y + aoe/2.0*0.7)
  set SCEffect[i*SC_MAX_EFFECTS_PER_INSTANCE + 1] = AddSpecialEffectLoc(CONTAMINATE_EFFECT, eLoc)
  call MoveLocation(eLoc, x + aoe/2.0*0.0, y + aoe/2.0*1.0)
  set SCEffect[i*SC_MAX_EFFECTS_PER_INSTANCE + 2] = AddSpecialEffectLoc(CONTAMINATE_EFFECT, eLoc)
  call MoveLocation(eLoc, x + aoe/2.0*(-0.7), y + aoe/2.0*0.7)
  set SCEffect[i*SC_MAX_EFFECTS_PER_INSTANCE + 3] = AddSpecialEffectLoc(CONTAMINATE_EFFECT, eLoc)
  call MoveLocation(eLoc, x + aoe/2.0*(-1.0), y + aoe/2.0*0.0)
  set SCEffect[i*SC_MAX_EFFECTS_PER_INSTANCE + 4] = AddSpecialEffectLoc(CONTAMINATE_EFFECT, eLoc)
  call MoveLocation(eLoc, x + aoe/2.0*(-0.7), y + aoe/2.0*(-0.7))
  set SCEffect[i*SC_MAX_EFFECTS_PER_INSTANCE + 5] = AddSpecialEffectLoc(CONTAMINATE_EFFECT, eLoc)
  call MoveLocation(eLoc, x + aoe/2.0*0.0, y + aoe/2.0*(-1.0))
  set SCEffect[i*SC_MAX_EFFECTS_PER_INSTANCE + 6] = AddSpecialEffectLoc(CONTAMINATE_EFFECT, eLoc)
  call MoveLocation(eLoc, x + aoe/2.0*0.7, y + aoe/2.0*(-0.7))
  set SCEffect[i*SC_MAX_EFFECTS_PER_INSTANCE + 7] = AddSpecialEffectLoc(CONTAMINATE_EFFECT, eLoc) 
  
  call RemoveLocation(eLoc)
  set eLoc = null

  set SCLoc[i] = loc
  set SCOwner[i] = GetOwningPlayer(c)
  set SCLevel[i] = GetUnitAbilityLevel(c, CONTAMINATE)
  set SCTimerExpiration[i] = CreateTimer()
  call TimerStart(SCTimerExpiration[i], CONTAMINATE_DURATION, false, function SCTimerExpirationCallback)
  set SCQuantity = SCQuantity + 1

  set c = null
  set loc = null
endfunction

function InitSpellContaminate takes nothing returns nothing
  local trigger t = CreateTrigger()
  local trigger t2 = CreateTrigger()

  call TriggerRegisterAnyUnitEventBJ(t, EVENT_PLAYER_UNIT_SPELL_EFFECT)
  call TriggerAddCondition(t, Condition(function SCOnCastFilter))
  call TriggerAddAction(t, function SCOnCastCallback)

  call TriggerRegisterAnyUnitEventBJ(t2, EVENT_PLAYER_UNIT_DEATH)
  call TriggerAddCondition(t2, Condition(function SCOnDeathFilter))
  call TriggerAddAction(t2, function SCOnDeathCallback)

  set t = null
  set t2 = null
endfunction
/* end SpellContaminate.j */
