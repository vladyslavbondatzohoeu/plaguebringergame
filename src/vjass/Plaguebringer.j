/* Begin Plaguebringer.j
Version: 1.1 */
globals
    constant player USER = Player(0)
endglobals
//! import "DifficultyDialog.j"
//! import "SpellCorpseGas.j"
//! import "SpellVoidShield.j"
//! import "SpellContaminate.j" 
//! import "SpellExtinguishLife.j" 
//! import "SpellAmalgamation.j"
//! import "SpellShadowLash.j"

globals     
    constant integer VOLGOFF = 'U600'
    
    constant integer SPELL_SCHOOL_UNHOLY = 1
    constant integer SPELL_SCHOOL_SHADOW = 2
    
    constant integer SPELL_UNHOLY_FIRST = CONTAMINATE
    constant integer SPELL_SHADOW_FIRST = SHADOW_LASH
    constant integer SPELL_UNHOLY_SECOND = CORPSE_GAS
    constant integer SPELL_SHADOW_SECOND = 'AUfu'
    constant integer SPELL_UNHOLY_THIRD = EXTINGUISH_LIFE
    constant integer SPELL_SHADOW_THIRD = 'AUdr'
    constant integer SPELL_UNHOLY_FOURTH = SUMMON_AMALGAMATION
    constant integer SPELL_SHADOW_FOURTH = 'AUdd'
endglobals

function VolgoffSetSpells takes unit u, integer slotOneSchool, integer slotOneRank, integer slotTwoSchool, integer slotTwoRank, integer slotThreeSchool, integer slotThreeRank, integer slotFourSchool, integer slotFourRank returns nothing  
    local integer spellPointsSpent = slotOneRank + slotTwoRank + slotThreeRank + slotFourRank
    
    call SetPlayerAbilityAvailable(USER, SPELL_UNHOLY_FIRST, false)
    call SetPlayerAbilityAvailable(USER, SPELL_UNHOLY_SECOND, false)
    call SetPlayerAbilityAvailable(USER, SPELL_UNHOLY_THIRD, false)
    call SetPlayerAbilityAvailable(USER, SPELL_UNHOLY_FOURTH, false)
    
    call SetPlayerAbilityAvailable(USER, SPELL_SHADOW_FIRST, false)
    call SetPlayerAbilityAvailable(USER, SPELL_SHADOW_SECOND, false)
    call SetPlayerAbilityAvailable(USER, SPELL_SHADOW_THIRD, false)
    call SetPlayerAbilityAvailable(USER, SPELL_SHADOW_FOURTH, false)
    
    call UnitRemoveAbility(u, SPELL_UNHOLY_FIRST)
    call UnitRemoveAbility(u, SPELL_UNHOLY_SECOND)
    call UnitRemoveAbility(u, SPELL_UNHOLY_THIRD)
    call UnitRemoveAbility(u, SPELL_UNHOLY_FOURTH)
    
    call UnitRemoveAbility(u, SPELL_SHADOW_FIRST)
    call UnitRemoveAbility(u, SPELL_SHADOW_SECOND)
    call UnitRemoveAbility(u, SPELL_SHADOW_THIRD)
    call UnitRemoveAbility(u, SPELL_SHADOW_FOURTH)
    
    if SPELL_SCHOOL_SHADOW == slotOneSchool then  
        call SetPlayerAbilityAvailable(USER, SPELL_SHADOW_FIRST, true)     
        if slotOneRank > 0 then    
            call UnitAddAbility(u, SPELL_SHADOW_FIRST)
            call SetUnitAbilityLevel(u, SPELL_SHADOW_FIRST, slotOneRank) 
        endif
    else        
        call SetPlayerAbilityAvailable(USER, SPELL_UNHOLY_FIRST, true)    
        if slotOneRank > 0 then                                              
            call UnitAddAbility(u, SPELL_UNHOLY_FIRST)
            call SetUnitAbilityLevel(u, SPELL_UNHOLY_FIRST, slotOneRank)    
        endif
    endif
    
    if SPELL_SCHOOL_SHADOW == slotTwoSchool then    
        call SetPlayerAbilityAvailable(USER, SPELL_SHADOW_SECOND, true)   
        if slotTwoRank > 0 then                                                 
            call UnitAddAbility(u, SPELL_SHADOW_SECOND)
            call SetUnitAbilityLevel(u, SPELL_SHADOW_SECOND, slotTwoRank) 
        endif 
    else    
        call SetPlayerAbilityAvailable(USER, SPELL_UNHOLY_SECOND, true)   
        if slotTwoRank > 0 then    
            call UnitAddAbility(u, SPELL_UNHOLY_SECOND)
            call SetUnitAbilityLevel(u, SPELL_UNHOLY_SECOND, slotTwoRank)   
        endif
    endif
    
    if SPELL_SCHOOL_SHADOW == slotThreeSchool then 
        call SetPlayerAbilityAvailable(USER, SPELL_SHADOW_THIRD, true)     
        if slotThreeRank > 0 then                                                
            call UnitAddAbility(u, SPELL_SHADOW_THIRD)
            call SetUnitAbilityLevel(u, SPELL_SHADOW_THIRD, slotThreeRank) 
        endif
    else       
        call SetPlayerAbilityAvailable(USER, SPELL_UNHOLY_THIRD, true)      
        if slotThreeRank > 0 then    
            call UnitAddAbility(u, SPELL_UNHOLY_THIRD)
            call SetUnitAbilityLevel(u, SPELL_UNHOLY_THIRD, slotThreeRank)    
        endif
    endif
    
    if SPELL_SCHOOL_SHADOW == slotFourSchool then    
        call SetPlayerAbilityAvailable(USER, SPELL_SHADOW_FOURTH, true)   
        if slotFourRank > 0 then                                            
            call UnitAddAbility(u, SPELL_SHADOW_FOURTH)
            call SetUnitAbilityLevel(u, SPELL_SHADOW_FOURTH, slotFourRank) 
        endif
    else  
        call SetPlayerAbilityAvailable(USER, SPELL_UNHOLY_FOURTH, true)     
        if slotFourRank > 0 then    
            call UnitAddAbility(u, SPELL_UNHOLY_FOURTH)
            call SetUnitAbilityLevel(u, SPELL_UNHOLY_FOURTH, slotFourRank) 
        endif
    endif    
    
    call ModifyHeroSkillPoints(u, bj_MODIFYMETHOD_SET, GetHeroLevel(u) - spellPointsSpent)
endfunction

function VolgoffSetSpellsUnholy takes unit u returns nothing
    call VolgoffSetSpells(u, SPELL_SCHOOL_UNHOLY, 0, SPELL_SCHOOL_UNHOLY, 0, SPELL_SCHOOL_UNHOLY, 0, SPELL_SCHOOL_UNHOLY, 0)
endfunction

function VolgoffSetSpellsShadow takes unit u returns nothing
    call VolgoffSetSpells(u, SPELL_SCHOOL_SHADOW, 0, SPELL_SCHOOL_SHADOW, 0, SPELL_SCHOOL_SHADOW, 0, SPELL_SCHOOL_SHADOW, 0)
endfunction

function VolgoffRefreshSpells takes gamecache db, string chapterLabel, unit u returns nothing    
    local integer slotOneRank = GetStoredInteger(db, chapterLabel, "slotOneRank")
    local integer slotTwoRank = GetStoredInteger(db, chapterLabel, "slotTwoRank") 
    local integer slotThreeRank = GetStoredInteger(db, chapterLabel, "slotThreeRank")
    local integer slotFourRank = GetStoredInteger(db, chapterLabel, "slotFourRank")     
    
    local integer slotOneSchool = GetStoredInteger(db, "Category", "slotOneSchool")
    local integer slotTwoSchool = GetStoredInteger(db, "Category", "slotTwoSchool") 
    local integer slotThreeSchool = GetStoredInteger(db, "Category", "slotThreeSchool")
    local integer slotFourSchool = GetStoredInteger(db, "Category", "slotFourSchool")

    if null == u or GetUnitTypeId(u) != VOLGOFF then
        return
    endif
    
    call VolgoffSetSpells(u, slotOneSchool, slotOneRank, slotTwoSchool, slotTwoRank, slotThreeSchool, slotThreeRank, slotFourSchool, slotFourRank)
endfunction

function RetrieveVolgoff takes gamecache db, string chapterLabel, real x, real y, real f returns unit
    local unit u = RestoreUnit(db, chapterLabel, "heroVolgoff", USER, x, y, f)
    
    if null == u then
        set u = CreateUnit(USER, VOLGOFF, x, y, f)
    endif 
    
    call VolgoffRefreshSpells(db, chapterLabel, u)
    
    return u
endfunction

function StoreVolgoff takes gamecache db, string chapterLabel, unit u returns nothing
    local integer a = 0
    local integer b = 0
    local integer c = 0
    
    if null == u or GetUnitTypeId(u) != VOLGOFF then
        return
    endif
    
    set a = GetUnitAbilityLevel(u, SPELL_UNHOLY_FIRST)
    set b = GetUnitAbilityLevel(u, SPELL_SHADOW_FIRST)
    set c = IMaxBJ(a, b)
    call StoreInteger(db, chapterLabel, "slotOneRank", c)
    
    set a = GetUnitAbilityLevel(u, SPELL_UNHOLY_SECOND)
    set b = GetUnitAbilityLevel(u, SPELL_SHADOW_SECOND)
    set c = IMaxBJ(a, b)
    call StoreInteger(db, chapterLabel, "slotTwoRank", c)
    
    set a = GetUnitAbilityLevel(u, SPELL_UNHOLY_THIRD)
    set b = GetUnitAbilityLevel(u, SPELL_SHADOW_THIRD) 
    set c = IMaxBJ(a, b)
    call StoreInteger(db, chapterLabel, "slotThreeRank", c)
    
    set a = GetUnitAbilityLevel(u, SPELL_UNHOLY_FOURTH)
    set b = GetUnitAbilityLevel(u, SPELL_SHADOW_FOURTH) 
    set c = IMaxBJ(a, b)
    call StoreInteger(db, chapterLabel, "slotFourRank", c)  
    
    call VolgoffRefreshSpells(db, chapterLabel, u)
    
    call StoreUnit(db, chapterLabel, "heroVolgoff", u)
endfunction

function InitPlaguebringer takes nothing returns nothing
    call InitSpellCorpseGas()
    call InitSpellVoidShield()
    call InitSpellContaminate()
    call InitSpellExtinguishLife()
    call InitSpellAmalgamation()
    call InitSpellShadowLash()
    
    call SetPlayerHandicapXPBJ(USER, 50.00 )
                             
    call CinematicModeBJ(true, GetPlayersAll())
    call CinematicFadeBJ(bj_CINEFADETYPE_FADEOUT, 0.00, "ReplaceableTextures\\CameraMasks\\Black_mask.blp", 0, 0, 0, 0)
endfunction
/* End Plaguebringer */
