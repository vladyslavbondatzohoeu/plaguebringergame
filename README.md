# Plaguebringer.
Custom campaign for WarCraft 3: Frozen Throne.
## Style.
Package name are capital letters of the file name.
For example, SpellCorpseGas.j becomes SCG prefix for 
functions in the script.
Public function must not be prefixed with the package name.
Private functions must be prefixed with the package name.
Maximum line length or width is 100 characters.
